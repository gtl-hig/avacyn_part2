// Fill out your copyright notice in the Description page of Project Settings.

#include "Avacyn.h"
#include "DroppedItem.h"
#include "Inventory.h"


// Sets default values for this component's properties
UInventory::UInventory()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	bHaveNewItems = true;
	// ...
	
	//misc
	ConstructorHelpers::FObjectFinder<UTexture2D> TextureRefrence(TEXT("Texture2D'/Game/Images/InventoryItems/misc/DefaultIcon.DefaultIcon'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/misc/DefaultIcon.DefaultIcon'"), TextureRefrence.Object);
	}
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/misc/flask.flask'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/misc/flask.flask'"), TextureRefrence.Object);
	}
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/misc/consumable.consumable'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/misc/consumable.consumable'"), TextureRefrence.Object);
	}
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/misc/currency.currency'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/misc/currency.currency'"), TextureRefrence.Object);
	}

	//armors
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Armors/boots.boots'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Armors/boots.boots'"), TextureRefrence.Object);
	}
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Armors/gloves.gloves'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Armors/gloves.gloves'"), TextureRefrence.Object);
	}
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Armors/armor.armor'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Armors/armor.armor'"), TextureRefrence.Object);
	}
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Armors/back.back'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Armors/back.back'"), TextureRefrence.Object);
	}
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Armors/helmet.helmet'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Armors/helmet.helmet'"), TextureRefrence.Object);
	}
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Armors/legs.legs'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Armors/legs.legs'"), TextureRefrence.Object);
	}
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Armors/shoulder.shoulder'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Armors/shoulder.shoulder'"), TextureRefrence.Object);
	}

	//jewelry
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Jewelry/amulet.amulet'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Jewelry/amulet.amulet'"), TextureRefrence.Object);
	}	
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Jewelry/ring.ring'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Jewelry/ring.ring'"), TextureRefrence.Object);
	}
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Jewelry/ancient_relic.ancient_relic'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Jewelry/ancient_relic.ancient_relic'"), TextureRefrence.Object);
	}

	//weapons
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/1h_axe.1h_axe'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/1h_axe.1h_axe'"), TextureRefrence.Object);
	}
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/2h_axe.2h_axe'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/2h_axe.2h_axe'"), TextureRefrence.Object);
	}
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/1h_mace.1h_mace'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/1h_mace.1h_mace'"), TextureRefrence.Object);
	}
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/2h_mace.2h_mace'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/2h_mace.2h_mace'"), TextureRefrence.Object);
	}
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/1h_sword.1h_sword'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/1h_sword.1h_sword'"), TextureRefrence.Object);
	}
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/2h_sword.2h_sword'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/2h_sword.2h_sword'"), TextureRefrence.Object);
	}
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/bow.bow'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/bow.bow'"), TextureRefrence.Object);
	}
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/dagger.dagger'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/dagger.dagger'"), TextureRefrence.Object);
	}
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/fist.fist'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/fist.fist'"), TextureRefrence.Object);
	}
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/quiver.quiver'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/quiver.quiver'"), TextureRefrence.Object);
	}
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/scythe.scythe'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/scythe.scythe'"), TextureRefrence.Object);
	}
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/shield.shield'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/shield.shield'"), TextureRefrence.Object);
	}
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/spellbook.spellbook'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/spellbook.spellbook'"), TextureRefrence.Object);
	}
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/staff.staff'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/staff.staff'"), TextureRefrence.Object);
	}
	TextureRefrence = ConstructorHelpers::FObjectFinder<UTexture2D>(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/elementalorb.elementalorb'"));
	if (TextureRefrence.Succeeded())
	{
		InventoryIcons.Add(TEXT("Texture2D'/Game/Images/InventoryItems/Weapons/elementalorb.elementalorb'"), TextureRefrence.Object);
	}


}


// Called when the game starts
void UInventory::BeginPlay()
{
	Super::BeginPlay();

	// ...

	/*
	//for testing items in inventory - will be removed
	FItem tempItem;
	tempItem.ID = FMath::Rand();
	tempItem.Type = ETypeOfItem::Boots;
	tempItem.Rarity = ETypeOfRarity::Legendary;
	tempItem.Name = TEXT("whoa, legendary!");
	tempItem.FlavorText = TEXT("such a wonderful item :3");
	tempItem.ImageRefrence = TEXT("Texture2D'/Game/Images/boots.boots'");
	//tempItem.ImageRefrence = TEXT("test");
	tempItem.MeshRefrence = TEXT("SkeletalMesh'/Game/CharacterModels/InfinityBladeCharacters/SK_CharM_Ram.SK_CharM_Ram'");
	tempItem.StatsOnItem.Add(ETypeOfStat::DEF_Armor, 20.1f);
	tempItem.StatsOnItem.Add(ETypeOfStat::DEF_BlockAmount, 20.11f);
	tempItem.StatsOnItem.Add(ETypeOfStat::DEF_BlockChance, 20.111f);
	tempItem.StatsOnItem.Add(ETypeOfStat::DEF_Thorns, 2000.1111f);
	tempItem.StatsOnItem.Add(ETypeOfStat::DI_AcidDamage, 20.11111f);
	tempItem.StatsOnItem.Add(ETypeOfStat::OFFP_AttackSpeedModifier, 20.111111f);
	tempItem.StatsOnItem.Add(ETypeOfStat::OFFP_PhysicalCritChance, 20.1111111f);
	ItemsInInventory.Add(tempItem);

	FItem Gloves;
	Gloves.ID = FMath::Rand();
	Gloves.Type = ETypeOfItem::Gloves;
	Gloves.Rarity = ETypeOfRarity::Unique;
	Gloves.Name = TEXT("gloves of skills");
	Gloves.FlavorText = TEXT("Once worn by the allmighty Slurpeedelic, conqueror of blueprints");
	Gloves.ImageRefrence = TEXT("Texture2D'/Game/Images/gloves.gloves'");
	Gloves.MeshRefrence = TEXT("SkeletalMesh'/Game/CharacterModels/InfinityBladeCharacters/SK_CharM_Ram.SK_CharM_Ram'");
	Gloves.StatsOnItem.Add(ETypeOfStat::OFFM_SpellCritChance, 100.0f);
	Gloves.StatsOnItem.Add(ETypeOfStat::OFFM_CastingSpeed, 100.0f);
	Gloves.StatsOnItem.Add(ETypeOfStat::OFFM_MagicPower, 100.0f);
	Gloves.StatsOnItem.Add(ETypeOfStat::OFFM_SpellCritDamage, 100.0f);
	Gloves.StatsOnItem.Add(ETypeOfStat::OFFM_BaseMagicPower, 100.0f);
	Gloves.StatsOnItem.Add(ETypeOfStat::RES_MaxHP, 100.0f);
	Gloves.StatsOnItem.Add(ETypeOfStat::RES_MaxMana, 100.0f);
	Gloves.StatsOnItem.Add(ETypeOfStat::RES_ManaRegenerationPerSecond, 100.0f);
	Gloves.StatsOnItem.Add(ETypeOfStat::RES_HPRegenerationPerSecond, 100.0f);
	ItemsInInventory.Add(Gloves);
	*/
}


// Called every frame
void UInventory::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	//GEngine->AddOnScreenDebugMessage(-1, 10005.0f, FColor::Blue, TEXT("INVENTORY EXISTS BAY"));
	// ...
}
/*
	DEPRICATED: Should not be used. Use AddItem(FItem item) instead.
*/
void UInventory::AddToInventory(FItem NewItem)
{
	ItemsInInventory.Add(NewItem);
}

TArray<FItem> UInventory::GetWholeInventory()
{

	return ItemsInInventory;
}

void UInventory::ApplySavedItems(TArray<FItem> SaveItems)
{
	FString sizeString = TEXT("");
	sizeString.AppendInt(ItemsInInventory.Num());
	//GEngine->AddOnScreenDebugMessage(-1, 10005.0f, FColor::Blue, TEXT("Size of Inventory before applying saved items: ") + sizeString);

	ItemsInInventory.Empty();

	//adding elements to array
	ItemsInInventory.Append(SaveItems);
	

	sizeString = TEXT("");
	sizeString.AppendInt(ItemsInInventory.Num());
	//GEngine->AddOnScreenDebugMessage(-1, 10005.0f, FColor::Blue, TEXT("Size of inventroy after applying saved items: ") + sizeString);
	bHaveNewItems = true;
}

TArray<float> UInventory::GetMapValues(FItem Target) {
	TArray<float> Data;
	Target.StatsOnItem.GenerateValueArray(Data);
	return Data;

};
TArray<ETypeOfStat> UInventory::GetMapNames(FItem Target)
{
	TArray<ETypeOfStat> Data;
	//Target->StatsOnItem.GenerateKeyArray(Data);
	Target.StatsOnItem.GetKeys(Data);
	return Data;
}


UTexture2D* UInventory::FindIcon(FString Path)
{
	auto Match = InventoryIcons.FindRef(Path);

	if (Match)
	{
		return Match;
	}
	else
	{
		return InventoryIcons.FindRef(TEXT("Texture2D'/Game/Images/InventoryItems/misc/DefaultIcon.DefaultIcon'"));
	}
}

void UInventory::BroadcastNewItem()
{
	bHaveNewItems = true;
}

void UInventory::CreateRandomItems(int32 NumberOfItemsToCreate)
{
	//create random shit here
}

TArray<FString> UInventory::GetIconNames()
{
	TArray<FString> Icons;
	InventoryIcons.GenerateKeyArray(Icons);
	return Icons;
}

void UInventory::CreateNewItem(FGuid uuid, ETypeOfItem Type, ETypeOfRarity Rarity, uint8 LevelRequirement, FString Name, FString FlavorText, FString ImageReference, FString MeshReference, TArray<ETypeOfStat> TypesOfStat, TArray<float> StatValues)
{
	//FText GetVictoryEnumAsString(EVictoryEnum::Type EnumValue)
	//{
	//	const UEnum* EnumPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("EVictoryEnum"), true);
	//	if (!EnumPtr) return NSLOCTEXT("Invalid", "Invalid", "Invalid");
	//
	//	return EnumPtr->GetDisplayNameText(EnumValue);
	//}
	FText temp;
	FItem NewItem;
//	GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Emerald, TEXT("ID: ") + FString::FromInt(Id));
	//NewItem.ID = Id;
	NewItem.GUID = uuid;
	GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Emerald, TEXT("Type: ")+ FString::FromInt(static_cast<int>(Type)));
	NewItem.Type = Type;
	GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Emerald, TEXT("Rarity: ") + FString::FromInt(static_cast<int>(Rarity)));
	NewItem.Rarity = Rarity;
	GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Emerald, TEXT("Name: ") + Name);
	NewItem.LevelRequirement = LevelRequirement;
	GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Emerald, TEXT("Level requirement: ") + FString::FromInt(static_cast<int>(LevelRequirement)));
	NewItem.Name = Name;
	GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Emerald, TEXT("FlavorText: ") + FlavorText);
	NewItem.FlavorText = FlavorText;
	GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Emerald, TEXT("Image Ref: ") + ImageReference);
	NewItem.ImageRefrence = ImageReference;
	GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Emerald, TEXT("Mesh Ref: ") + MeshReference);

	NewItem.MeshRefrence = MeshReference;
	for (int32 i = 0; i < TypesOfStat.Num(); i++)
	{
		NewItem.StatsOnItem.Add(TypesOfStat[i], StatValues[i]);
	}

	ItemsInInventory.Add(NewItem);
	GEngine->AddOnScreenDebugMessage(-1, 10005.0f, FColor::Emerald, TEXT("CREATED NEW ITEM"));
	//AAvacynPlayerCharacter* myOwner = Cast<AAvacynPlayerCharacter>(GetOwner());

	bHaveNewItems = true;
	}


FItem UInventory::GetInventoryItem(FGuid ItemID)
{
	//return *ItemsInInventory.FindByPredicate([ItemID](const FItem& ItemInArray) { return ItemInArray.GUID == ItemID; });
	for (auto Item : ItemsInInventory)
	{
		if (Item.GUID == ItemID)
		{
			return Item;
		}
	}

	//do this better? :/
	FItem FailedToFind;
	return FailedToFind;
}

bool UInventory::DropItem(FItem ItemToDrop)
{
	//auto Result = ItemsInInventory.Find(ItemToDrop);
	auto Result = ItemsInInventory.Find(ItemToDrop);
	if (Result != INDEX_NONE)
	{
		FActorSpawnParameters SpawnInfo;
		FVector Rotation;
		SpawnInfo.Instigator = Cast<APawn>(GetOwner());
		APawn* MyOwner = Cast<APawn>(GetOwner());
		auto SpawnedObject = MyOwner->GetWorld()->SpawnActor<ADroppedItem>(ADroppedItem::StaticClass(),SpawnInfo);
		SpawnedObject->InitializeDroppedItemData(ItemToDrop, MyOwner->GetActorLocation());
		
		
		//removes the item from inventory container.
		if (ItemsInInventory.Remove(ItemToDrop) > 0)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 10005.0f, FColor::Emerald, TEXT("Dropped item"));
			return true;
		}
	}
	return false;
}

bool UInventory::AddItem(FItem ItemToAdd)
{
	ItemsInInventory.AddUnique(ItemToAdd);
	if (ItemsInInventory.Find(ItemToAdd) != INDEX_NONE)
	{
		return true;
	}
	return false;
}