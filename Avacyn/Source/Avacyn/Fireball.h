// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseProjectile.h"
#include "Fireball.generated.h"

/**
 * 
 */
UCLASS()
class AVACYN_API AFireball : public ABaseProjectile
{
	GENERATED_BODY()

public:

	AFireball();

	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION()
	virtual void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;
	
	virtual void HitActor(AActor* OtherActor) override;

	UParticleSystemComponent* ParticleComponent;

	UParticleSystem* ImpactParticleSystem;
};
