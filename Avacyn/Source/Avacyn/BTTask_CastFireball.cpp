// Fill out your copyright notice in the Description page of Project Settings.

#include "Avacyn.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "AvacynCharacter.h"
#include "EnemyAI.h"
#include "BaseEnemy.h"
#include "BTTask_CastFireball.h"




EBTNodeResult::Type UBTTask_CastFireball::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AEnemyAI* CharacterPlayerController = Cast<AEnemyAI>(OwnerComp.GetAIOwner());

	AAvacynCharacter* Enemy = Cast<AAvacynCharacter>(OwnerComp.GetBlackboardComponent()->GetValue<UBlackboardKeyType_Object>(CharacterPlayerController->EnemyKeyID));

	float DesiredRangeToTarget = 500.0f;

	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, TEXT("AI Try To Cast Fireball!"));

	if (Enemy)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, FString::Printf(TEXT("DistanceToEnemy: %f"), CharacterPlayerController->GetPawn()->GetDistanceTo(Enemy)));
		if (CharacterPlayerController->LineOfSightTo(Enemy) && CharacterPlayerController->GetPawn()->GetDistanceTo(Enemy) < DesiredRangeToTarget + 50.0f)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Turquoise, TEXT("AI Cast Fireball!"));
			Cast<ABaseEnemy>(CharacterPlayerController->GetPawn())->CastSpell(ESpellName::Fireball_rank1, Enemy->GetActorLocation());
			return EBTNodeResult::Succeeded;
		}
	}
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("AI target ERROR"));
	return EBTNodeResult::Failed;
}