// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "TalentFromFile.h"
#include "Enums.h"
#include "Structs.h"
#include "Components/ActorComponent.h"
#include "SpellDetails.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AVACYN_API USpellDetails : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USpellDetails();
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void InitializeData(TArray<FDATAFROMFILE> TalentsToSplit);

	//Find functions for converting FString to Enum of different types
	ESpellName FindTalentName(FString Name);
	ETypeOfTalent FindTypeOfTalent(FString TalentName);
	ETypeOfDamage FindTypeOfDamage(FString damage);
	ETypeOfCC	  FindTypeOfCC(FString cctype);
	ETypeOfStatusEffect FindTypeOfStatusEffect(FString statusEffect);
	ETypeOfSpecialEffect FindTypeOfSpecialEffect(FString specialEffect);
	ETypeOfDOT FindTypeOfDOT(FString dotType);
	ETypeOfStat FindTypeOfStat(FString stat);
	ETypeOfPassive FindTypeOfPassive(FString TypeOfPassive);
	
	//converts string to the correct enum
	// credits to Unreal wiki - url: https://wiki.unrealengine.com/Enums_For_Both_C%2B%2B_and_BP
	template <typename EnumType>
	static FORCEINLINE EnumType GetEnumValueFromString(const FString& EnumName, const FString& String) //hadde referanse..
	{
		UEnum* Enum = FindObject<UEnum>(ANY_PACKAGE, *EnumName, true);
		if (!Enum)
		{
			return EnumType(0);
		}
		return (EnumType)Enum->FindEnumIndex(FName(*String));
	}

	//convert enum to String
	template<typename TEnum>
	static FORCEINLINE FString GetEnumValueToString(const FString& Name, TEnum Value)
	{
		const UEnum* enumPtr = FindObject<UEnum>(ANY_PACKAGE, *Name, true);
		if (!enumPtr)
		{
			return FString("Invalid");
		}
		return enumPtr->GetEnumName((int32)Value);
	}


	//test to show the component is reachable from character
	FString test;
	UFUNCTION(BlueprintCallable, Category = "SpellDetails")
	FORCEINLINE TMap<ETalentName, FTalentInfo>				GetTalentInfo()			{ return TalentInfo; };
	
	UFUNCTION(BlueprintCallable, Category = "SpellDetails")
	FORCEINLINE TMap<ESpellName, FSpellInfo>				GetSpellInfo()			{ return SpellInfo; };
	
	UFUNCTION(BlueprintCallable, Category = "SpellDetails")
	FORCEINLINE TMap<ESpellName, FAugmentInfo>				GetAugmentInfo()		{ return AugmentInfo; };
	
	UFUNCTION(BlueprintCallable, Category = "SpellDetails")
	FORCEINLINE TMap<ESpellName, FCCInfo>					GetCCInfo()				{ return CCInfo; };

	UFUNCTION(BlueprintCallable, Category = "SpellDetails")	
	FORCEINLINE TMap<ESpellName, FStatusEffectInfo>		GetStatusEffectInfo()	{ return StatusEffectInfo; };
	
	UFUNCTION(BlueprintCallable, Category = "SpellDetails")
	FORCEINLINE TMap<ESpellName, FDOTInfo>					GetDOTInfo()			{ return DOTInfo; };
	
	UFUNCTION(BlueprintCallable, Category = "SpellDetails")
	FORCEINLINE TMap<ESpellName, FPassiveInfo>				GetPassiveInfo()		{ return PassiveInfo; };
	
	UFUNCTION(BlueprintCallable, Category = "SpellDetails")
	FORCEINLINE TMap<ESpellName, FAugmentsPossibleInfo>	GetAugmentsAvailable()	{ return AugmentsAvailable; };

	//TMultiMap<ETalentName, FSpellInfo>
	TMap<ETalentName, FTalentInfo> TalentInfo;
	TMap<ESpellName, FSpellInfo> SpellInfo;
	TMap<ESpellName, FAugmentInfo> AugmentInfo;
	TMap<ESpellName, FCCInfo> CCInfo;
	TMap<ESpellName, FStatusEffectInfo> StatusEffectInfo;
	TMap<ESpellName, FDOTInfo> DOTInfo;
	TMap<ESpellName, FPassiveInfo> PassiveInfo;
	TMap<ESpellName, FAugmentsPossibleInfo> AugmentsAvailable;
};
