#pragma once

#include "Enums.h"
#include <vector>
#include "Structs.generated.h"




USTRUCT(BlueprintType)
struct FItem
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, Category = "FItem")
	FGuid GUID;
	UPROPERTY(BlueprintReadWrite, Category = "FItem")
	ETypeOfItem Type;
	UPROPERTY(BlueprintReadWrite, Category = "FItem")
	ETypeOfRarity Rarity;
	UPROPERTY(BlueprintReadWrite, Category = "FItem")
	uint8 LevelRequirement;
	UPROPERTY(BlueprintReadWrite, Category = "FItem")
	FString Name;
	UPROPERTY(BlueprintReadWrite, Category = "FItem")
	FString FlavorText;
	UPROPERTY(BlueprintReadWrite, Category = "FItem")
	FString ImageRefrence;
	UPROPERTY(BlueprintReadWrite, Category = "FItem")
	FString MeshRefrence;
	UPROPERTY(BlueprintReadWrite, Category = "FItem")
	TMap<ETypeOfStat, float> StatsOnItem;

	FORCEINLINE bool operator==(const FItem &Other) const
	{
		return GUID == Other.GUID;
	}

};


USTRUCT(BlueprintType)
struct FProjectileBehaviorData
{
	GENERATED_BODY()

public:
	// Time the projectile will live, or distance it will travel before disappearing
	UPROPERTY(BlueprintReadWrite, Category = "FProjectileData")
		float MaxTravelDistance;

	// The speed of the projectile
	UPROPERTY(BlueprintReadWrite, Category = "FProjectileData")
		float ProjectileSpeed;

	// The Aoe size of the projectile
	UPROPERTY(BlueprintReadWrite, Category = "FProjectileData")
		float AoESize;
};

USTRUCT(BlueprintType)
struct FProjectileDamageData
{
	GENERATED_BODY()

public:
	// The damage the projectile will inflict of each type
	UPROPERTY(BlueprintReadWrite, Category = "FProjectileData")
		TMap<ETypeOfDamage, float> Damage;

	// The amount of each type that will penetrate defense
	UPROPERTY(BlueprintReadWrite, Category = "FProjectileData")
		TMap<ETypeOfDamage, float> DamagePenetrationPercentage;

	// The level of the character that cast the spell
	UPROPERTY(BlueprintReadWrite, Category = "FProjectileData")
		int32 InstigatorLevel;

	// The elite damage modifier of the character that cast the spell
	UPROPERTY(BlueprintReadWrite, Category = "FProjectileData")
		int32 EliteDamageModifier;
	
	// The character that cast the spell
	UPROPERTY(BlueprintReadWrite, Category = "FProjectileData")
		AActor* Instigator;
};

USTRUCT(BlueprintType)
struct FTalentInfo
{

	GENERATED_BODY()

	//used to display name for tooltips
	UPROPERTY(BlueprintReadOnly, Category = "FTalentInfo")
		ESpellName Name;

	//used to give a description of the talent in the tooltip
	UPROPERTY(BlueprintReadOnly, Category = "FTalentInfo")
		FString Description;

	//Used to find the correct icon to display when showing the talent tree and actionbar
	UPROPERTY(BlueprintReadOnly, Category = "FTalentInfo")
		FString Icon;
	//simple variable to make it easy to display which kind of talent it is for tooltip
	UPROPERTY(BlueprintReadOnly, Category = "FTalentInfo")
		ETypeOfTalent TypeOfTalent;

	//does the talent have a global effect?
	UPROPERTY(BlueprintReadOnly, Category = "FTalentInfo")
		bool bIsGlobal;
	//if it's a talent, it will appear in the talent tree. (variable to help differentiate from talents and augmented spells)
	UPROPERTY(BlueprintReadOnly, Category = "FTalentInfo")
		bool bIsTalent;

	//determines where in the talent tree the given talent will be placed. also determines restrictions on obtaining the talent.
	UPROPERTY(BlueprintReadOnly, Category = "FTalentInfo")
		int TalentTier;

	UPROPERTY(BlueprintReadOnly, Category = "FTalentInfo")
		ETypeOfTalentTree TalentTree;
};

USTRUCT(BlueprintType)
struct FSpellInfo
{
	/*
	This container keeps track of all the base information which most offensive spells have in common.
	*/
	GENERATED_BODY()
	//stores the key for this struct to make it easier to switch tooltip in skill window
	UPROPERTY(BlueprintReadOnly, Category = "FSpellInfo")
	ESpellName SpellName;
	//which type of damage the spell is supposed to deal. can be modified if augmented
	UPROPERTY(BlueprintReadOnly, Category = "FSpellInfo")
	ETypeOfDamage TypeOfDamage;
	//base damage min and max. makes the game feel more fluid as you will never deal the same amount of damage every time you cast this spell
	UPROPERTY(BlueprintReadOnly, Category = "FSpellInfo")
		float BaseDamageMin;
	UPROPERTY(BlueprintReadOnly, Category = "FSpellInfo")
		float BaseDamageMax;
	//determines if the spell should fire off multiple projectiles
	UPROPERTY(BlueprintReadOnly, Category = "FSpellInfo")
		int NumberOfProjectiles;
	//determines how fast the spell travels
	UPROPERTY(BlueprintReadOnly, Category = "FSpellInfo")
		int ProjectileSpeed;
	//determines how long it takes to cast the given spell
	UPROPERTY(BlueprintReadOnly, Category = "FSpellInfo")
		float CastTime;
	//determines how far the spell will travel before it destroys itself
	UPROPERTY(BlueprintReadOnly, Category = "FSpellInfo")
		int Range;
	//determines if the spell should destroy upon impact or keep traveling until it reaches max range.
	UPROPERTY(BlueprintReadOnly, Category = "FSpellInfo")
		bool bCanPassThroughEnemies;
	//determines the interval in which the player can cast the given spell.
	UPROPERTY(BlueprintReadOnly, Category = "FSpellInfo")
		float Cooldown;
	//determines if the spell will be recursive. if >0, it will recreate itself that many times.
	UPROPERTY(BlueprintReadOnly, Category = "FSpellInfo")
		int NumberOfBouncesOrChains;
	//determines how large the collisionbox for the spell is.
	UPROPERTY(BlueprintReadOnly, Category = "FSpellInfo")
		int AoESize;
	//determines how much effect stats such as Life on hit should have when using the spell
	UPROPERTY(BlueprintReadOnly, Category = "FSpellInfo")
		float StatEfficiency;
	//determines which type of resource to use when casting spell
	UPROPERTY(BlueprintReadOnly, Category = "FSpellInfo")
		ETypeOfResource ResourceType;
	//determines the amount of resource the spellcasting component should use
	UPROPERTY(BlueprintReadOnly, Category = "FSpellInfo")
		int ResourceCost;
	//string representing what class the spell should spawn.
	UPROPERTY(BlueprintReadOnly, Category = "FSpellInfo")
		FString ClassToSpawn;


};

USTRUCT(BlueprintType)
struct FAugmentInfo
{
	/*
	This container keeps track of the given augment. which spells it can be attached to and which effects the
	given augment embodies.
	*/
	GENERATED_BODY()

		//Determines which spells this augment can affect
		UPROPERTY(BlueprintReadOnly, Category = "FAugmentInfo")
		TArray<ETalentName> SpellsToAugment;

	UPROPERTY(BlueprintReadOnly, Category = "FAugmentInfo")
		float BaseDamageMin;
	UPROPERTY(BlueprintReadOnly, Category = "FAugmentInfo")
		float BaseDamageMax;
	UPROPERTY(BlueprintReadOnly, Category = "FAugmentInfo")
		int NumberOfProjectiles;
	UPROPERTY(BlueprintReadOnly, Category = "FAugmentInfo")
		int ProjectileSpeed;
	UPROPERTY(BlueprintReadOnly, Category = "FAugmentInfo")
		int CastTime;
	UPROPERTY(BlueprintReadOnly, Category = "FAugmentInfo")
		int Range;
	UPROPERTY(BlueprintReadOnly, Category = "FAugmentInfo")
		bool bCanPassThroughEnemies;
	UPROPERTY(BlueprintReadOnly, Category = "FAugmentInfo")
		int Cooldown;
	UPROPERTY(BlueprintReadOnly, Category = "FAugmentInfo")
		int NumberOfBouncesOrChains;
	UPROPERTY(BlueprintReadOnly, Category = "FAugmentInfo")
		int AoESize;


};

USTRUCT(BlueprintType)
struct FCCInfo
{
	/*
	If a spell or effect has a CC attached to it, it can be found by looking up in the CC map with the spell name.
	if an instance of this object is returned, the spell or effect has a CC effect attached to it.
	*/
	GENERATED_BODY()

		UPROPERTY(BlueprintReadOnly, Category = "FCCInfo")
		ETypeOfCC CCType;
	UPROPERTY(BlueprintReadOnly, Category = "FCCInfo")
		float ChanceToCC;
	UPROPERTY(BlueprintReadOnly, Category = "FCCInfo")
		float CCDuration;



};

USTRUCT(BlueprintType)
struct FStatusEffectInfo
{
	/*
	This container keeps track of the given augment. which spells it can be attached to and which effects the
	given augment embodies.
	*/
	GENERATED_BODY()

		UPROPERTY(BlueprintReadOnly, Category = "FStatusEffectInfo")
		ETypeOfStatusEffect TypeOfEffect;

	UPROPERTY(BlueprintReadOnly, Category = "FStatusEffectInfo")
		float EffectDuration;

	UPROPERTY(BlueprintReadOnly, Category = "FStatusEffectInfo")
		TMap<ETypeOfStat, float> StatsModifiedByEffect;

	UPROPERTY(BlueprintReadOnly, Category = "FStatusEffectInfo")
		ETypeOfSpecialEffect SpecialEffect;

};

USTRUCT(BlueprintType)
struct FDOTInfo
{
	/*
	This container keeps track of the given augment. which spells it can be attached to and which effects the
	given augment embodies.
	*/
	GENERATED_BODY()

		UPROPERTY(BlueprintReadOnly, Category = "FDOTInfo")
		ETypeOfDOT TypeOfDOT;

	UPROPERTY(BlueprintReadOnly, Category = "FDOTInfo")
		float ChanceToApply;

	UPROPERTY(BlueprintReadOnly, Category = "FDOTInfo")
		float DOTDuration;

	UPROPERTY(BlueprintReadOnly, Category = "FDOTInfo")
		float DOTTickInterval;

	UPROPERTY(BlueprintReadOnly, Category = "FDOTInfo")
		float DOTMinDamage;

	UPROPERTY(BlueprintReadOnly, Category = "FDOTInfo")
		float DOTMaxDamage;
};

USTRUCT(BlueprintType)
struct FPassiveInfo
{
	/*
	All passive effects gained through either talents, ancient relics, and legendary equipment are detailed with this struct.
	*/
	GENERATED_BODY()
		//which type of passive: Forgot what this was supposed to do.
		UPROPERTY(BlueprintReadOnly, Category = "FPassiveInfo")
		ETypeOfPassive TypeOfPassive;

	//which statistics modifications to apply when the given passive is in effect.
	UPROPERTY(BlueprintReadOnly, Category = "FPassiveInfo")
		TMap<ETypeOfStat, float> StatsToApply;

	///TODO: make special passive info? passives that does weird and unusual stuff. 


};

USTRUCT(BlueprintType)
struct FAugmentsPossibleInfo
{
	/*
	This container keeps track of the given augment. which spells it can be attached to and which effects the
	given augment embodies.
	*/
	GENERATED_BODY()

		//Determines which spells this augment can affect
		UPROPERTY(BlueprintReadOnly, Category = "FAugmentsPossibleInfo")
		TArray<ETalentName> SpellsAugmentable;
};

//A struct holding information about each talent within the specific talent tree
USTRUCT(BlueprintType)
struct FTalentTreeAllocationInfo
{

	GENERATED_BODY()
		//an enum making it easy to know which talent tree these allocated points belong to.
	UPROPERTY(BlueprintReadWrite, Category = "FTalentTreeAllocationInfo")
	ETypeOfTalentTree AssociatedTalentTree;
	//Contains all the points. between 0 and 3, indicating how many talent points spent in the specific talent.
		//TMap<ETalentName, uint32> PointsAllocated;
	UPROPERTY(BlueprintReadWrite, Category = "FTalentTreeAllocationInfo")
		TMap<ETalentName, uint8> PointsAllocated;
		//TArray<ETalentName> Talent;
	//UPROPERTY(BlueprintReadWrite, Category = "FTalentTreeAllocationInfo")
	//TArray<uint8> PointsAllocated;
};

USTRUCT(BlueprintType)
struct FEquipmentSlot
{
	GENERATED_BODY()

		//UPROPERTY(BlueprintReadWrite, Category = "Equipment Slot")
		//FItem CurrentEquippedItem;
	UPROPERTY(BlueprintReadWrite, Category = "Equipment Slot")
	FGuid EquippedItemID;
	
	UPROPERTY(BlueprintReadWrite, Category = "Equipment Slot")
	ETypeOfEquipmentSlot EquipableType;

	//UPROPERTY(BlueprintReadWrite, Category = "Equipment Slot")
	//Array<ETypeOfItem> EquippableItemType;
};

USTRUCT(BlueprintType)
struct FItemModifier
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly, Category = "Loot")
	FString NameModifier;

	UPROPERTY(BlueprintReadOnly, Category = "Loot")
	TArray<ETypeOfStat>  AvailableStats;
};