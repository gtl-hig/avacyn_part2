// Fill out your copyright notice in the Description page of Project Settings.

#include "Avacyn.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "AvacynCharacter.h"
#include "EnemyAI.h"
#include "BaseEnemy.h"
#include "BTService_CheckForPlayer.h"




UBTService_CheckForPlayer::UBTService_CheckForPlayer()
{
	bCreateNodeInstance = true;
}

void UBTService_CheckForPlayer::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	AEnemyAI* CharacterPlayerController = Cast<AEnemyAI>(OwnerComp.GetAIOwner());
	if (CharacterPlayerController)
	{
		AAvacynCharacter* Enemy = Cast<AAvacynCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());

		if (CharacterPlayerController->LineOfSightTo(Enemy) && CharacterPlayerController->GetPawn()->GetDistanceTo(Enemy) < 3000.0f)
		{
			if (Enemy)
			{
				OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Object>(CharacterPlayerController->EnemyKeyID, Enemy);
			}
		}
	}
}