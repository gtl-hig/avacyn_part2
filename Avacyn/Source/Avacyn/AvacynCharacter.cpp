// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "Avacyn.h"
#include "AvacynCharacter.h"
#include "Damageable.h"
#include "Healable.h"
#include "Statistics.h"
#include "SpellCastingComponent.h"
#include "Inventory.h"
#include "EquippedItems.h"
#include "TalentTree.h"
#include "AvacynGameInstance.h"
#include "SpellDetails.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"

AAvacynCharacter::AAvacynCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;
	GetCharacterMovement()->MaxWalkSpeed = 1000.0f;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bAbsoluteRotation = true; // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 1800.f;
	CameraBoom->RelativeRotation = FRotator(-60.f, 0.f, 0.f);
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

										  // Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	CameraReference = TopDownCameraComponent;
	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/TopDownCPP/Blueprints/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	// Add Statistics component to character
	Statistics = CreateDefaultSubobject<UStatistics>(TEXT("Statistics"));
	this->AddOwnedComponent(Statistics);

	// Add Spellcasting component to character
	SpellCastingComponent = CreateDefaultSubobject<USpellCastingComponent>(TEXT("Spells"));
	this->AddOwnedComponent(SpellCastingComponent);

	// add inventory component to character
	Inventory = CreateDefaultSubobject<UInventory>(TEXT("Inventory"));
	this->AddOwnedComponent(Inventory);

	// add equippeditems component to character
	EquippedItems = CreateDefaultSubobject<UEquippedItems>(TEXT("EquippedItems"));
	this->AddOwnedComponent(EquippedItems);

	// add Talent Tree component to character
	TalentTree = CreateDefaultSubobject<UTalentTree>(TEXT("TalentTree"));
	this->AddOwnedComponent(TalentTree);
}

void AAvacynCharacter::Tick(float DeltaSeconds)
{

    Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params;
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}

	

	//test works :)

	//auto derp = UGameplayStatics::GetGameInstance(GetWorld());
	//auto test = Cast<UAvacynGameInstance>(derp);
	//auto comp = test->GetSpellDetailsComponent();
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, comp->test);
}

void AAvacynCharacter::CastSpell(ESpellName Spell, FVector TargetLocation)
{
	SpellCastingComponent->CastSpell(Spell, TargetLocation);
}

//implementation for the interface Damageable
bool AAvacynCharacter::ReceiveDamage_Implementation(FProjectileDamageData ProjectileDamageData)
{
	Statistics->ReceiveDamage(ProjectileDamageData);

	return true; //required to return something
}

//implementation for the interface Healable
bool AAvacynCharacter::ReceiveHealing_Implementation()
{


	return true; //required to return something
}

void AAvacynCharacter::EnterSpellcastingState(float Time)
{
	GetCharacterMovement()->StopActiveMovement();
	
	FHitResult Hit;
	UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHitResultUnderCursor(ECC_Visibility, false, Hit);
	FVector TargetLocation = Hit.ImpactPoint;
	FVector PawnLocation = GetActorLocation();
	TargetLocation.Z += (PawnLocation.Z - TargetLocation.Z);
	FVector Rotation(TargetLocation - PawnLocation);
	Rotation.Normalize();

	SetActorRotation(Rotation.Rotation());
	
	FTimerDelegate TimerDelegate = FTimerDelegate::CreateUObject(this, &AAvacynCharacter::ExitSpellcastingState);
	GetWorld()->GetTimerManager().SetTimer(SpellcastingState, TimerDelegate, Time, false);
}

void AAvacynCharacter::ExitSpellcastingState()
{

}