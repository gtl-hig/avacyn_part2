// Fill out your copyright notice in the Description page of Project Settings.

#include "Avacyn.h"
#include "SpellDetails.h"


// Sets default values for this component's properties
USpellDetails::USpellDetails()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.SetTickFunctionEnable(true);
	test = TEXT("test");
	// READ SPELLDATA FROM FILE
}



// Called when the game starts
void USpellDetails::BeginPlay()
{
	Super::BeginPlay();

	// ...

}


// Called every frame
void USpellDetails::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Orange, TEXT("does this show?"));

	// ...
	UE_LOG(LogTemp, Warning, TEXT("USPELL DETAILS:"));
	UE_LOG(LogTemp, Warning, TEXT("Number of entries in TalentInfo: %i"), TalentInfo.Num());
	for (auto each : TalentInfo)
	{
		FString temp = "TalentInfo name:";
		temp.Append(GetEnumValueToString<ESpellName>("ESpellName", each.Value.Name));
		UE_LOG(LogTemp, Warning, TEXT("%s"), &temp.GetCharArray());
	}
	UE_LOG(LogTemp, Warning, TEXT("Number of entries in SpellInfo: %i"), SpellInfo.Num());
	for (auto spell : SpellInfo)
	{
		FString temp = "Spellinfo cast time: ";
		temp.AppendInt(spell.Value.CastTime);
		UE_LOG(LogTemp, Warning, TEXT("%s"), &temp.GetCharArray());

	}
}

void USpellDetails::InitializeData(TArray<FDATAFROMFILE> TalentsToSplit)
{
	UE_LOG(LogTemp, Warning, TEXT("Initializing spell details - inside initializeData function"));
	// READ SPELLDATA FROM FILE
	for (auto Talent : TalentsToSplit)
	{
		//FTALENT INFO
		FTalentInfo tempTalentInfo;
		FString tempTalentNameString = Talent.Name;
		tempTalentNameString = tempTalentNameString.Replace(TEXT(" "), TEXT("_"));
		tempTalentNameString = tempTalentNameString.Replace(TEXT("!"), TEXT(""));

		ETalentName tempTalentName = GetEnumValueFromString<ETalentName>("ETalentName", tempTalentNameString);
		FString enumTalentName = "ETalentName";
		FString SpellNameString = Talent.Name;
		SpellNameString.Append("_rank");
		SpellNameString.AppendInt(Talent.Rank);
		SpellNameString = SpellNameString.Replace(TEXT(" "), TEXT(""));
		SpellNameString = SpellNameString.Replace(TEXT("!"), TEXT(""));

		tempTalentInfo.Name = GetEnumValueFromString<ESpellName>("ESpellName", SpellNameString);
		tempTalentInfo.Description = Talent.Description;
		tempTalentInfo.Icon = Talent.Icon;
		tempTalentInfo.TypeOfTalent = GetEnumValueFromString<ETypeOfTalent>("ETypeOfTalent", Talent.TypeOfTalent);
		tempTalentInfo.bIsGlobal = Talent.bIsGlobal;
		tempTalentInfo.bIsTalent = Talent.bIsTalent;
		tempTalentInfo.TalentTier = Talent.Tier;

		tempTalentInfo.TalentTree = GetEnumValueFromString<ETypeOfTalentTree>("ETypeOfTalentTree", Talent.TalentTree);
		if (!TalentInfo.Find(tempTalentName))
		{
			TalentInfo.Add(tempTalentName, tempTalentInfo);
		}

		//SPELL INFO
		FSpellInfo tempSpellInfo;
		tempSpellInfo.SpellName = tempTalentInfo.Name;
		tempSpellInfo.TypeOfDamage = GetEnumValueFromString<ETypeOfDamage>("ETypeOfDamage", Talent.TypeOfDamage);
		tempSpellInfo.BaseDamageMin = Talent.BaseDamageMin;
		tempSpellInfo.BaseDamageMax = Talent.BaseDamageMax;
		tempSpellInfo.NumberOfProjectiles = Talent.NumberOfProjectiles;
		tempSpellInfo.ProjectileSpeed = Talent.ProjectileSpeed;
		tempSpellInfo.CastTime = Talent.CastTime;
		tempSpellInfo.Range = Talent.Range;

		//TODO: add this field to spreadsheet
		tempSpellInfo.bCanPassThroughEnemies = true;

		tempSpellInfo.ResourceType = GetEnumValueFromString<ETypeOfResource>("ETypeOfResource", Talent.ResourceType);
		tempSpellInfo.ResourceCost = Talent.ResourceCost;
		tempSpellInfo.Cooldown = Talent.Cooldown;
		tempSpellInfo.NumberOfBouncesOrChains = Talent.NumberOfBouncesAndChains;
		tempSpellInfo.AoESize = Talent.BaseAoeSize;
		tempSpellInfo.ClassToSpawn = Talent.ClassToSpawn;
		SpellInfo.Add(tempTalentInfo.Name, tempSpellInfo);
		//AUGMENTINFO???

		//CC INFO
		//FCCInfo tempCCInfo;
		//tempCCInfo.CCType = FindTypeOfCC(Talent.TypeOfCC);
		//tempCCInfo.ChanceToCC = Talent.ChanceToApplyCC;
		//tempCCInfo.CCDuration = Talent.CCDuration;

		//Status effect INFO
		//FStatusEffectInfo tempStatusEffect;
		//tempStatusEffect.TypeOfEffect = FindTypeOfStatusEffect(Talent.TypeOfEffect);
		//tempStatusEffect.EffectDuration = 10;
		//tempStatusEffect.StatsModifiedByEffect
		//tempStatusEffect.SpecialEffect = FindTypeOfSpecialEffect(Talent.type)

		//DOT INFO
		//FDOTInfo tempDOTInfo;
		//tempDOTInfo.TypeOfDOT = FindTypeOfDOT(Talent.TypeOfDOT);
		//tempDOTInfo.ChanceToApply = Talent.ChanceToApply;
		//tempDOTInfo.DOTDuration = Talent.DOTDuration;
		//tempDOTInfo.DOTTickInterval = Talent.DOTTickInterval;
		//tempDOTInfo.DOTMinDamage = Talent.DOTDamageMin;
		//tempDOTInfo.DOTMaxDamage = Talent.DOTDamageMax;

		//PASSIVE INFO
		if (Talent.TypeOfPassive.Len() > 0)
		{
			FPassiveInfo tempPassiveInfo;
			tempPassiveInfo.TypeOfPassive = FindTypeOfPassive(Talent.TypeOfPassive);
		}
		//tempPassiveInfo.StatsToApply


		//AUGMENTS AVAILABLE???
	}
	UE_LOG(LogTemp, Warning, TEXT("done initializing"));
	UE_LOG(LogTemp, Warning, TEXT("size of talents: %i"), TalentInfo.Num());
	UE_LOG(LogTemp, Warning, TEXT("size of spells: %i"), SpellInfo.Num());

}
ESpellName USpellDetails::FindTalentName(FString Name)
{
	if (Name.Compare(TEXT("Fireball_rank1")))
	{

	}

	//TODO: implement find function
	return ESpellName::Fireball_rank1;
}
ETypeOfTalent USpellDetails::FindTypeOfTalent(FString TalentName)
{
	//TODO: Implement find function
	return ETypeOfTalent::Active;
}
ETypeOfDamage USpellDetails::FindTypeOfDamage(FString damage)
{
	//TODO: Implement find function
	return ETypeOfDamage::Fire;
}
ETypeOfCC USpellDetails::FindTypeOfCC(FString cctype)
{
	//TODO: Implement find function
	return ETypeOfCC::Freeze;
}
ETypeOfStatusEffect USpellDetails::FindTypeOfStatusEffect(FString statusEffect)
{
	//TODO: Implement find function
	return ETypeOfStatusEffect::Buff;
}
ETypeOfSpecialEffect USpellDetails::FindTypeOfSpecialEffect(FString specialEffect)
{
	//TODO: Implement find function
	return ETypeOfSpecialEffect::NONE;
}
ETypeOfDOT USpellDetails::FindTypeOfDOT(FString dotType)
{
	//TODO: Implement find function
	return ETypeOfDOT::Poison;
}
ETypeOfPassive USpellDetails::FindTypeOfPassive(FString TypeOfPassive)
{
	return ETypeOfPassive::Bleeding;
}
ETypeOfStat USpellDetails::FindTypeOfStat(FString stat)
{
	return ETypeOfStat::DEF_Armor;
}