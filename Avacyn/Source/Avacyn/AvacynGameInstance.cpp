// Fill out your copyright notice in the Description page of Project Settings.

#include "Avacyn.h"
#include "SpellDetails.h"
#include "AvacynGameInstance.h"
//#include "TalentFromFile.h"




UAvacynGameInstance::UAvacynGameInstance(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
	//  new save default settings
	bIsNewCharacter = false;
	//CharacterName = TEXT("Player1");
	//CharacterFileName = TEXT("Character_100");
	//CharacterFileSlot = 0;
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> MeshContainer(TEXT("SkeletalMesh'/Game/CharacterModels/InfinityBladeCharacters/SK_CharM_Barbarous.SK_CharM_Barbarous'"));
	if (MeshContainer.Succeeded())
	{
		CharacterSkeletalMesh = MeshContainer.Object;
	}
	//  load character default settings
	CharacterFileName = TEXT("Character_99");
	CharacterFileSlot = 0;
	CharacterName = TEXT("Player 1");
	// debug settings
	bIsDebugMode = true;


	// add Talent Tree component to character
	SpellDetails = CreateDefaultSubobject<USpellDetails>(TEXT("Spell Details"));
	//this->AddOwnedComponent(SpellDetails);


	ConstructorHelpers::FObjectFinder<UDataTable> MyDataTableObj(TEXT("DataTable'/Game/Talent_Info.Talent_Info'"));
	if (MyDataTableObj.Succeeded())
	{
		UDataTable* TalentsFromFile = MyDataTableObj.Object;
		UE_LOG(LogTemp, Warning, TEXT("Data Table found"));
		if (TalentsFromFile)
		{
			TArray<FDATAFROMFILE> AllTalents;
			FString ContextString;
			TArray<FName> RowNames;
			RowNames = TalentsFromFile->GetRowNames();
			for (auto& name : RowNames)
			{
				FDATAFROMFILE* row = TalentsFromFile->FindRow<FDATAFROMFILE>(name, ContextString);
				if (row)
				{
					AllTalents.Add(*row);
				}
			}
			//TODO: remove when done implementing
			//for (auto kake : AllTalents)
			//{
			//	FString test = kake.Name;
			//	UE_LOG(LogTemp, Warning, TEXT("Spell name: %s"), *test);
			//}
			if (AllTalents.Num() < 1)
			{
				UE_LOG(LogTemp, Error, TEXT("NO TALENTS WERE LOADED"));
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("Initializing spell details"));

				//if the data was actually loaded (should always happen)
				InitializeSpellDetails(AllTalents);
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Can't actually access the data"));
		}

	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Data Table not found"));
	}

}


bool UAvacynGameInstance::IsNewCharacter()
{
	return bIsNewCharacter;
}

void UAvacynGameInstance::InitializeSpellDetails(TArray<FDATAFROMFILE> AllTalents)
{
	// initialize Spelldetails
	SpellDetails = CreateDefaultSubobject<USpellDetails>(TEXT("SpellDetails"));
	SpellDetails->InitializeData(AllTalents);
}

bool UAvacynGameInstance::IsDebugMode()
{
	return bIsDebugMode;
}

/************************************************************************/
/* GET FUNCTIONS                                                        */
/************************************************************************/
FString UAvacynGameInstance::GetCharacterFileName()
{
	return CharacterFileName;
}
int32 UAvacynGameInstance::GetCharacterFileSlot()
{
	return CharacterFileSlot;
}
USkeletalMesh* UAvacynGameInstance::GetCharacterSkeletalMesh()
{
	return CharacterSkeletalMesh;
}
FString UAvacynGameInstance::GetCharacterName()
{
	return CharacterName;
}

/************************************************************************/
/* SET FUNCTIONS                                                        */
/************************************************************************/

void UAvacynGameInstance::SetCharacterFileName(FString NewSaveName)
{
	CharacterFileName = NewSaveName;
}
void UAvacynGameInstance::SetCharacterFileSlot(int32 NewSaveSlot)
{
	CharacterFileSlot = NewSaveSlot;
}
void UAvacynGameInstance::SetCharacterName(FString newName)
{
	CharacterName = newName;
}

void UAvacynGameInstance::SetCharacterSkeletalMesh(USkeletalMesh * NewSkeletalMesh)
{
	CharacterSkeletalMesh = NewSkeletalMesh;
}
