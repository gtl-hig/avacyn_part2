// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "Enums.h"
#include <vector>
#include "SpellCastingComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AVACYN_API USpellCastingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USpellCastingComponent();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	// Call this to make the owner of this component try to cast a spell
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Spells")
	bool CastSpell(ESpellName Spell, FVector TargetLocation);

	// Helper map to map a type of damage to the resistance for that type of damage
	TMap<ETypeOfDamage, ETypeOfStat> DamageToPenetrationMap;
	
	void FinishCastingSpell(ESpellName Spell, FVector TargetLocation);

	void StopCastingSpell();

private:

	// Calculates the damage a spell will deal
	template<typename T>
	void CalculateDamageData(T Character, FProjectileDamageData &NewProjectileDamageData, float &Damage, ESpellName Spell);

	// Checks if the owner has enough resource to cast a spell, and uses that recource if it has
	template<typename T>
	bool UseResource(T Character, ESpellName Spell);

	// Puts the spell cast on cooldown
	template<typename T>
	void PutSpellOnCooldown(T Character, ESpellName Spell);

	// Whether or not the owner character has access to a spell
	TMap<ESpellName, bool> HasAccessToSpell;

	// Keeps track of each spells cooldown
	TMap<ESpellName, float> RemainingSpellCooldown;

	// Called every tick to reduce remaining cooldown on spells
	void TickSpellCooldowns(float DeltaTime);

	// Changes a spells remaining cooldown to the spesified value
	void SetSpellCooldown(ESpellName Spell, float Duration);

	// Changes whether or not the owner has access to a spell
	void SetSpellAccess(ESpellName Spell, bool bNewHasAccessToSpell);

	FTimerHandle CurrentSpellcast;
};
