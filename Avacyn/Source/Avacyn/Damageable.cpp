// Fill out your copyright notice in the Description page of Project Settings.

#include "Avacyn.h"
#include "Damageable.h"


// This function does not need to be modified.
UDamageable::UDamageable(const class FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}

// Add default functionality here for any IDamageable functions that are not pure virtual.
