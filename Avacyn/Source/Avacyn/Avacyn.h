// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#ifndef __AVACYN_H__
#define __AVACYN_H__

#include "Engine.h"
#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
#include "Runtime/UMG/Public/Slate/SObjectWidget.h"
#include "Runtime/UMG/Public/IUMGModule.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"

DECLARE_LOG_CATEGORY_EXTERN(LogAvacyn, Log, All);


#endif
