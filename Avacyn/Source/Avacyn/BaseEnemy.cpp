// Fill out your copyright notice in the Description page of Project Settings.

#include "Avacyn.h"
#include "Damageable.h"
#include "Healable.h"
#include "Statistics.h"
#include "BaseEnemy.h"
//#include "Components/WidgetComponent.h"

// Sets default values
ABaseEnemy::ABaseEnemy()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Add Statistics component to character
	Statistics = CreateDefaultSubobject<UStatistics>(TEXT("Statistics"));
	this->AddOwnedComponent(Statistics);

	//IMPLEMENT WHEN RELATIVE POSITION BUG IS FIXED BY EPIC
	//Test = CreateDefaultSubobject<UWidgetComponent>(TEXT("test"));
	//this->AddOwnedComponent(Test);

	// Add Spellcasting component to character
	SpellCastingComponent = CreateDefaultSubobject<USpellCastingComponent>(TEXT("Spells"));
	this->AddOwnedComponent(SpellCastingComponent);
}

// Called when the game starts or when spawned
void ABaseEnemy::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABaseEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABaseEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

//implementation for the interface Damageable
bool ABaseEnemy::ReceiveDamage_Implementation(FProjectileDamageData ProjectileDamageData)
{
	Statistics->ReceiveDamage(ProjectileDamageData); 

	return true; //required to return something so Unreal engine doesn't treat this as an event
}

//implementation for the interface Healable
bool ABaseEnemy::ReceiveHealing_Implementation()
{


	return true; //required to return something so Unreal engine doesn't treat this as an event
}

void ABaseEnemy::Die()
{
	GetSpellCastingComponent()->StopCastingSpell();
	this->Destroy();
}

void ABaseEnemy::CastSpell(ESpellName Spell, FVector TargetLocation)
{
	SpellCastingComponent->CastSpell(Spell, TargetLocation);
}