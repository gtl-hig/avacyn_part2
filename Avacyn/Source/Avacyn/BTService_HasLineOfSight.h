// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BehaviorTree/BTService.h"
#include "BTService_HasLineOfSight.generated.h"

/**
 * 
 */
UCLASS()
class AVACYN_API UBTService_HasLineOfSight : public UBTService
{
	GENERATED_BODY()
	
public:
	UBTService_HasLineOfSight();

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	
	
};
