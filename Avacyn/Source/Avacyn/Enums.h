#pragma once

#include "Enums.generated.h"

const uint8 NUM_TYPES_OF_DAMAGE = 11;
const uint8 NUM_TYPES_OF_WEAPONS = 16;

UENUM(BlueprintType)
enum class ETypeOfDamage : uint8
{
	Physical	UMETA(DisplayName = "Physical"),
	Fire		UMETA(DisplayName = "Fire"),
	Frost		UMETA(DisplayName = "Frost"),
	Lightning	UMETA(DisplayName = "Lightning"),
	Holy		UMETA(DisplayName = "Holy"),
	Shadow		UMETA(DisplayName = "Shadow"),
	Nature		UMETA(DisplayName = "Nature"),
	Poison		UMETA(DisplayName = "Poison"),
	Acid		UMETA(DisplayName = "Acid"),
	Bleed		UMETA(DisplayName = "Bleed"),
	Chaos		UMETA(DisplayName = "Chaos")
};


UENUM(BlueprintType)
enum class ETypeOfWeapon : uint8
{
	Sword_1H	UMETA(DisplayName = "One Handed Sword"),
	Dagger		UMETA(DisplayName = "Dagger"),
	Mace_1H		UMETA(DisplayName = "One Handed Mace"),
	Fist		UMETA(DisplayName = "Fist"),
	Axe_1H		UMETA(DisplayName = "One Handed Axe"),
	Shield_MH	UMETA(DisplayName = "Main Hand Shield"),
	Sword_2H	UMETA(DisplayName = "Two Handed Sword"),
	Mace_2H		UMETA(DisplayName = "Two Handed Mace"),
	Staff		UMETA(DisplayName = "Staff"),
	Bow			UMETA(DisplayName = "Bow"),
	Axe_2H		UMETA(DisplayName = "Two Handed Axe"),
	Scythe		UMETA(DisplayName = "Scythe"),
	Shield_OH	UMETA(DisplayName = "Off Hand Shield"),
	Quiver		UMETA(DisplayName = "Quiver"),
	ElementalOrb UMETA(DisplayName = "Elemental Orb"),
	Spellbook	UMETA(DisplayName = "Spellbook")
};



UENUM(BlueprintType)
enum class ETypeOfItem : uint8
{
	Back				UMETA(DisplayName = "Back"),
	Boots				UMETA(DisplayName = "Boots"),
	Chest				UMETA(DisplayName = "Chest"),
	Gloves				UMETA(DisplayName = "Gloves"),
	Helmet				UMETA(DisplayName = "Helmet"),
	Legs				UMETA(DisplayName = "Legs"),
	Shoulder			UMETA(DisplayName = "Shoulder"),
	Amulet				UMETA(DisplayName = "Amulet"),
	Ancient_Relic		UMETA(DisplayName = "Ancient Relic"),
	Ring				UMETA(DisplayName = "Ring"),
	Flask				UMETA(DisplayName = "Flask"),
	Consumable			UMETA(DisplayName = "Consumable"),
	Currency			UMETA(DisplayName = "Currency"),
	Axe_1H				UMETA(DisplayName = "One Handed Axe"),
	Axe_2H				UMETA(DisplayName = "Two Handed Axe"),
	Mace_1H				UMETA(DisplayName = "One Handed Mace"),
	Mace_2H				UMETA(DisplayName = "Two Handed Mace"),
	Sword_1H			UMETA(DisplayName = "One Handed Sword"),
	Sword_2H			UMETA(DisplayName = "Two Handed Sword"),
	Shield_MH			UMETA(DisplayName = "Main Hand Shield"),
	Shield_OH			UMETA(DisplayName = "Off Hand Shield"),
	Bow					UMETA(DisplayName = "Bow"),
	Staff				UMETA(DisplayName = "Staff"),
	Scythe				UMETA(DisplayName = "Scythe"),
	Dagger				UMETA(DisplayName = "Dagger"),
	Fist				UMETA(DisplayName = "Fist"),
	Quiver				UMETA(DisplayName = "Quiver"),
	ElementalOrb		UMETA(DisplayName = "Elemental Orb"),
	Spellbook			UMETA(DisplayName = "Spellbook")
};

UENUM(BlueprintType)
enum class ETypeOfRarity : uint8
{
	Magical			UMETA(DisplayName = "Magical"),
	Rare			UMETA(DisplayName = "Rare"),
	Epic			UMETA(DisplayName = "Epic"),
	Legendary		UMETA(DisplayName = "Legendary"),
	Unique			UMETA(DisplayName = "Unique")
};

UENUM(BlueprintType)
enum class ETypeOfSpell : uint8
{
	BaseSpell		UMETA(DisplayName = "Base Spell")
};

UENUM(BlueprintType)
enum class ETypeOfStat : uint8
{
	/* resources */
	// Int - current maximum possible health for the pawn
	RES_MaxHP									UMETA(DisplayName = "Maximum HP"),
	// Int - current health for the pawn
	RES_CurrentHP								UMETA(DisplayName = "Current HP"),
	//Float - how much life the pawn gains per second
	RES_HPPerSecond								UMETA(DisplayName = "HP Per Second"),
	// Int - current maximum possible mana for the owned pawn
	RES_MaxMana									UMETA(DisplayName = "Maxmimum Mana"),
	// Int - Current mana for the pawn
	RES_CurrentMana								UMETA(DisplayName = "Current Mana"),
	//Float - how much mana the pawn gains per second
	RES_ManaPerSecond							UMETA(DisplayName = "Mana Per Second"),
	// Int - defensive stat, reduces physical damage taken
	DEF_Armor									UMETA(DisplayName = "Armor"),
	// Int - deals damage back to the instigator when struct by physical damage
	DEF_Thorns									UMETA(DisplayName = "Thorns"),
	// Int - deals damage back tothe instigator when struct by magical damage
	DEF_MagicalBacklash							UMETA(DisplayName = "Magical Backlash"),
	// Float - % value
	DEF_PhysicalSave							UMETA(DisplayName = "Physical Save"),
	// Float - % value
	DEF_MentalSave								UMETA(DisplayName = "Mental Save"),
	// Float - % value, determine whether or not the owner is hit by ability or not.
	DEF_DodgeChance								UMETA(DisplayName = "Dodge Chance"),
	// Float - % value, chance to block an attack
	DEF_BlockChance								UMETA(DisplayName = "Block Chance"),
	// Int - reduces the amount of damage taken by this value on successful block
	DEF_BlockAmount								UMETA(DisplayName = "Block Amount"),

	/* defensive / life regen 			*/
	// int - determines how much life the pawn gains per attack
	LO_LifeOnHit								UMETA(DisplayName = "Life On Hit"),
	// int - determines how much life the pawn gains when blocking an attack.
	LO_LifeOnBlock								UMETA(DisplayName = "Life On Block"),
	// int - Determines how much life the pawn gains when dodging an attack.
	LO_LifeOnDodge								UMETA(DisplayName = "Life On Dodge"),
	// int - Determines how much life the pawn gains when killing an enemy.
	LO_LifeOnKill								UMETA(DisplayName = "Life On Kill"),
	// int - Determines how much life the pawn regenerates per second.
	LO_LifePerSecond							UMETA(DisplayName = "Life Per Second"),
	// int - Determines how much life per resource(mana) the pawn spends when using abilities.
	LO_LifePerResourceSpent						UMETA(DisplayName = "Life Per Resource Spent"),

	/* utility							*/
	// float - % value - determines how much faster than base speed the pawn moves.
	UTIL_MovementSpeed							UMETA(DisplayName = "Movement Speed"),
	// float - % value - Reduces the cooldown on abilities.
	UTIL_CooldownReduction						UMETA(DisplayName = "Cooldown Reduction"),

	/* utility / offensive 				*/
	// float - % value - increases radius of spells and attacks
	UTIL_AoeSize								UMETA(DisplayName = "AoE Size Modifier"),
	// int - increases the travel distance of projectile attacks
	UTIL_ProjectileRange						UMETA(DisplayName = "Projectile Range"),
	// int - increases the maximum distance from pawn a spell can be cast
	UTIL_SpellRange								UMETA(DisplayName = "Spell Range"),
	// float - % - increases radius of cleave attacks
	UTIL_CleaveRadius							UMETA(DisplayName = "Cleave Radius"),
	// float - % - chance to freeze enemies on hit
	UTIL_ChanceToFreezeOnHit					UMETA(DisplayName = "Chance To Freeze On Hit"),
	// float - % - chance to stun enemies on hit
	UTIL_ChanceToStunOnHit						UMETA(DisplayName = "Chance To Stun On Hit"),
	// float - % - chance to slow enemies on hit
	UTIL_ChanceToSlowOnHit						UMETA(DisplayName = "Chance To Slow On Hit"),
	// float - % - increases duration of curses cast
	UTIL_CurseDuration							UMETA(DisplayName = "Curse Duration"),
	// float - % - increases effect of curses cast
	UTIL_CurseEffect							UMETA(DisplayName = "Curse Effect"),
	// int - increases maximum number of curses the pawn can apply to the same foe
	UTIL_MaxNumberOfCurses						UMETA(DisplayName = "Max Number Of Curses"),

	/* utility / defensive 			*/
	// float - % - increases the potency of potions used
	POT_PotionStrength							UMETA(DisplayName = "Potion Strength"),
	// float - % - increases duration of potion effects
	POT_PotionDuration							UMETA(DisplayName = "Potion Duration"),

	/* minion 						*/
	// float - % - increase movement speed of owned minions
	MINI_MinionMovementSpeed					UMETA(DisplayName = "Minion Movement Speed"),
	// float - % - increase aoe radius of attacks done by minions owned by pawn
	MINI_MinionAoeSize							UMETA(DisplayName = "Minion Aoe Size"),
	// float - % - increases aoe damage done by minions owned by pawn
	MINI_MinionAoeDamage						UMETA(DisplayName = "Minion Aoe Damage"),
	// float - % - increases damage done by minions owned by pawn
	MINI_MinionDamage							UMETA(DisplayName = "Minion Damage"),

	/* offensive 					*/
	// float - % - increases damage done to elite enemies
	OFF_EliteDamage								UMETA(DisplayName = "Elite Damage"),
	// float - % - increases damage done for aoe attacks
	OFF_AoeDamage								UMETA(DisplayName = "AoE Damage"),
	// int - increases travel speed for projectiles
	OFF_ProjectileSpeed							UMETA(DisplayName = "Projectile Speed"),
	// int - increase amount of times a spell chains or bounces
	OFF_AdditionalChainAndBounceTargets			UMETA(DisplayName = "Additional Chain And Bounce Targets"),
	// float - % - chance to apply ignite to enemies hit by attacks
	OFF_ChanceToIgniteOnHit						UMETA(DisplayName = "Chance To Ignite On Hit"),
	// float - % - chance to apply bleed to enemies hit by attacks
	OFF_ChanceToBleedOnHit						UMETA(DisplayName = "Chance To Bleed On Hit"),
	// float - % - chance to apply poison to enemies hit by attacks
	OFF_ChanceToPoisonOnHit						UMETA(DisplayName = "Chance To Poison On Hit"),
	// ??
	OFF_BurnBaseDamage							UMETA(DisplayName = "Burn Damage"),
	// ??
	OFF_BleedBaseDamage							UMETA(DisplayName = "Chance To Bleed On Hit"),
	// ??
	OFF_PoisonBaseDamage						UMETA(DisplayName = "Poison Damage"),

	/* offensive / magic 			*/
	// float - % - base power of magical attacks
	OFFM_BaseMagicPower							UMETA(DisplayName = "Base Magic Power"),
	// float - % - increases magical power of spells
	OFFM_MagicPower								UMETA(DisplayName = "Magic Power"),
	// float - % - chance for spell to critically hit
	OFFM_SpellCritChance						UMETA(DisplayName = "Spell Critical Chance"),
	// float - % - damage done by spell when successfully critical hit
	OFFM_SpellCritDamage						UMETA(DisplayName = "Spell Critical Damage"),
	// float - % - speed at which spells are cast
	OFFM_CastingSpeed							UMETA(DisplayName = "Casting Speed"),

	/* offensive / physical */
	// float - % - chance for physical attack to critically hit
	OFFP_PhysicalCritChance						UMETA(DisplayName = "Physical Critical Chance"),
	// float - % - damage done by attacks when successfully critical hit
	OFFP_PhysicalCritDamage						UMETA(DisplayName = "Physical Critical Damage"),
	// int - minimum damage done by physical attack
	OFFP_PhysicalBaseDamageMin					UMETA(DisplayName = "Minimum Physical Damage"),
	// int - maximum damage done by physical attack
	OFFP_PhysicalBaseDamageMax					UMETA(DisplayName = "Maximum Physical Damage"),
	// float - % - determines how fast the pawn attacks
	OFFP_AttackSpeedModifier					UMETA(DisplayName = "Attack Speed increase"),
	// float - time between attacks
	OFFP_CooldownBetweenAttacks					UMETA(DisplayName = "Cooldown Between Attacks"),
	// float - % - determines how fast the pawn attacks while dual wielding one handed weapons
	OFFP_DualWieldingAttackSpeed				UMETA(DisplayName = "Dual Wielding Attack Speed"),

	/* damage modifiers / offensive */
	// float - % - increase damage done by attacks with the given damage type
	DI_PhysicalDamage							UMETA(DisplayName = "Physical Damage"),
	DI_FireDamage								UMETA(DisplayName = "Fire Damage"),
	DI_FrostDamage								UMETA(DisplayName = "Frost Damage"),
	DI_LightningDamage							UMETA(DisplayName = "Lightning Damage"),
	DI_HolyDamage								UMETA(DisplayName = "Holy Damage"),
	DI_ShadowDamage								UMETA(DisplayName = "Shadow Damage"),
	DI_NatureDamage								UMETA(DisplayName = "Nature Damage"),
	DI_PoisonDamage								UMETA(DisplayName = "Poison Damage"),
	DI_AcidDamage								UMETA(DisplayName = "Acid Damage"),
	DI_BleedDamage								UMETA(DisplayName = "Bleed Damage"),
	DI_ChaosDamage								UMETA(DisplayName = "Chaos Damage"),

	// float - % - ignores resistance for attacks done with the given type. 
	// resistance ignored is the same as damage type of attack
	DP_DamagePenetrationPhysical				UMETA(DisplayName = "Physical Penetration"),
	DP_DamagePenetrationFire					UMETA(DisplayName = "Fire Penetration"),
	DP_DamagePenetrationFrost					UMETA(DisplayName = "Frost Penetration"),
	DP_DamagePenetrationLightning				UMETA(DisplayName = "Lightning Penetration"),
	DP_DamagePenetrationHoly					UMETA(DisplayName = "Holy Penetration"),
	DP_DamagePenetrationShadow					UMETA(DisplayName = "Shadow Penetration"),
	DP_DamagePenetrationNature					UMETA(DisplayName = "Nature Penetration"),
	DP_DamagePenetrationPoison					UMETA(DisplayName = "Poison Penetration"),
	DP_DamagePenetrationAcid					UMETA(DisplayName = "Acid Penetration"),

	// float - % - damage increases while wielding the given type of weapon
	DIW_DamageIncreaseWithSword_1H				UMETA(DisplayName = "Damage with 1h Sword"),
	DIW_DamageIncreaseWithDagger				UMETA(DisplayName = "Damage with Dagger"),
	DIW_DamageIncreaseWithMace_1H				UMETA(DisplayName = "Damage with 1h Mace"),
	DIW_DamageIncreaseWithFist					UMETA(DisplayName = "Damage with Fist"),
	DIW_DamageIncreaseWithAxe_1H				UMETA(DisplayName = "Damage with 1h Axe"),
	DIW_DamageIncreaseWithShield_MH				UMETA(DisplayName = "Damage with MainHand Shield"),
	DIW_DamageIncreaseWithSword_2H				UMETA(DisplayName = "Damage with 2h Sword"),
	DIW_DamageIncreaseWithMace_2H				UMETA(DisplayName = "Damage with 2h Mace"),
	DIW_DamageIncreaseWithStaff					UMETA(DisplayName = "Damage with Staff"),
	DIW_DamageIncreaseWithBow					UMETA(DisplayName = "Damage with Bow"),
	DIW_DamageIncreaseWithAxe_2H				UMETA(DisplayName = "Damage with 2h Axe"),
	DIW_DamageIncreaseWithScythe				UMETA(DisplayName = "Damage with Scythe"),
	DIW_DamageIncreaseWithShield_OH				UMETA(DisplayName = "Damage with Offhand Shield"),
	DIW_DamageIncreaseWithQuiver				UMETA(DisplayName = "Damage with Quiver"),
	DIW_DamageIncreaseWithElementalOrb			UMETA(DisplayName = "Damage with Elemental Orb"),
	DIW_DamageIncreaseWithSpellbook				UMETA(DisplayName = "Damage with Spellbook"),

	// float - % - damage taken reduced of type
	DR_DamageResistancePhysical					UMETA(DisplayName = "Physical Resistance"),
	DR_DamageResistanceFire						UMETA(DisplayName = "Fire Resistance"),
	DR_DamageResistanceFrost					UMETA(DisplayName = "Frost Resistance"),
	DR_DamageResistanceLightning				UMETA(DisplayName = "Lightning Resistance"),
	DR_DamageResistanceHoly						UMETA(DisplayName = "Holy Resistance"),
	DR_DamageResistanceShadow					UMETA(DisplayName = "Shadow Resistance"),
	DR_DamageResistanceNature					UMETA(DisplayName = "Nature Resistance"),
	DR_DamageResistancePoison					UMETA(DisplayName = "Poison Resistance"),
	DR_DamageResistanceAcid						UMETA(DisplayName = "Acid Resistance"),
	DR_DamageResistanceBleed					UMETA(DisplayName = "Bleed Resistance"),
	DR_DamageResistanceChaos					UMETA(DisplayName = "Chaos Resistance"),

	//float - % - damage taken reduced of all types of damage
	DR_DamageReduction							UMETA(DisplayName = "All Damage Reduction")
};


UENUM(BlueprintType)
enum class ETalentName : uint8
{
	///FIRE TALENTS
	Fireball						UMETA(DisplayName = "Fireball"),
	Touch_Of_Fire					UMETA(DisplayName = "Touch of Fire"),
	Elemental_Proliferate			UMETA(DisplayName = "Elemental Proliferate"),
	Third_Degree_Burn				UMETA(DisplayName = "3rd degree burn"),
	Firewalker						UMETA(DisplayName = "Firewalker"),
	Flamestrike						UMETA(DisplayName = "Flamestrike"),
	Hot_Hot_Hot						UMETA(DisplayName = "Hot Hot Hot!"),
	Blazing_Barrier					UMETA(DisplayName = "Blazing Barrier"),
	Embers							UMETA(DisplayName = "Embers"),
	Firewave						UMETA(DisplayName = "Firewave"),
	Dragons_Breath					UMETA(DisplayName = "Flamebreath"),
	Pyroblast						UMETA(DisplayName = "Pyroblast"),
	ARMAGEDDON						UMETA(DisplayName = "ARMAGEDDON!"),
	Infernal_Form					UMETA(DisplayName = "Infernal Form"),
	Ravaging_Blaze					UMETA(DisplayName = "Ravaging Blaze"),
	Fire_Rune						UMETA(DisplayName = "Fire Rune"),
	Fire_Contagion					UMETA(DisplayName = "Fire Contagion"),
	Detonate_Self					UMETA(DisplayName = "Detonate Self"),
	Blazing_Augmentation			UMETA(DisplayName = "Blazing Augmentation"),
	Continuous_Flames				UMETA(DisplayName = "Continuous Flames"),
	Detonate_Dead					UMETA(DisplayName = "Detonate Dead"),
	Magma_Orb						UMETA(DisplayName = "Magma Orb")

	///FROST TALENTS
	///<name> TALENTS
	///<name> TALENTS
	///<name> TALENTS
	///<name> TALENTS
	///<name> TALENTS

};

UENUM(BlueprintType)
enum class ESpellName : uint8
{
	TouchOfFire_rank1 UMETA(DisplayName = "TouchOfFire_rank1"),
	TouchOfFire_rank2 UMETA(DisplayName = "TouchOfFire_rank2"),
	TouchOfFire_rank3 UMETA(DisplayName = "TouchOfFire_rank3"),
	Fireball_rank1 UMETA(DisplayName = "Fireball_rank1"),
	Fireball_rank2 UMETA(DisplayName = "Fireball_rank2"),
	Fireball_rank3 UMETA(DisplayName = "Fireball_rank3"),
	ThirdDegreeBurn_rank1 UMETA(DisplayName = "ThirdDegreeBurn_rank1"),
	ThirdDegreeBurn_rank2 UMETA(DisplayName = "ThirdDegreeBurn_rank2"),
	ThirdDegreeBurn_rank3 UMETA(DisplayName = "ThirdDegreeBurn_rank3"),
	Flamestrike_rank1 UMETA(DisplayName = "Flamestrike_rank1"),
	Flamestrike_rank2 UMETA(DisplayName = "Flamestrike_rank2"),
	Flamestrike_rank3 UMETA(DisplayName = "Flamestrike_rank3"),
	BlazingBarrier_rank1 UMETA(DisplayName = "BlazingBarrier_rank1"),
	BlazingBarrier_rank2 UMETA(DisplayName = "BlazingBarrier_rank2"),
	BlazingBarrier_rank3 UMETA(DisplayName = "BlazingBarrier_rank3"),
	Firewave_rank1 UMETA(DisplayName = "Firewave_rank1"),
	Firewave_rank2 UMETA(DisplayName = "Firewave_rank2"),
	Firewave_rank3 UMETA(DisplayName = "Firewave_rank3"),
	DragonsBreath_rank1 UMETA(DisplayName = "DragonsBreath_rank1"),
	DragonsBreath_rank2 UMETA(DisplayName = "DragonsBreath_rank2"),
	DragonsBreath_rank3 UMETA(DisplayName = "DragonsBreath_rank3"),
	ARMAGEDDON_rank1 UMETA(DisplayName = "ARMAGEDDON_rank1"),
	ARMAGEDDON_rank2 UMETA(DisplayName = "ARMAGEDDON_rank2"),
	ARMAGEDDON_rank3 UMETA(DisplayName = "ARMAGEDDON_rank3"),
	RavagingBlaze_rank1 UMETA(DisplayName = "RavagingBlaze_rank1"),
	RavagingBlaze_rank2 UMETA(DisplayName = "RavagingBlaze_rank2"),
	RavagingBlaze_rank3 UMETA(DisplayName = "RavagingBlaze_rank3"),
	FireRune_rank1 UMETA(DisplayName = "FireRune_rank1"),
	FireRune_rank2 UMETA(DisplayName = "FireRune_rank2"),
	FireRune_rank3 UMETA(DisplayName = "FireRune_rank3"),
	DetonateSelf_rank1 UMETA(DisplayName = "DetonateSelf_rank1"),
	DetonateSelf_rank2 UMETA(DisplayName = "DetonateSelf_rank2"),
	DetonateSelf_rank3 UMETA(DisplayName = "DetonateSelf_rank3"),
	DetonateDead_rank1 UMETA(DisplayName = "DetonateDead_rank1"),
	DetonateDead_rank2 UMETA(DisplayName = "DetonateDead_rank2"),
	DetonateDead_rank3 UMETA(DisplayName = "DetonateDead_rank3"),
	MagmaOrb_rank1 UMETA(DisplayName = "MagmaOrb_rank1"),
	MagmaOrb_rank2 UMETA(DisplayName = "MagmaOrb_rank2"),
	MagmaOrb_rank3 UMETA(DisplayName = "MagmaOrb_rank3"),
	ElementalProliferate_rank1 UMETA(DisplayName = "ElementalProliferate_rank1"),
	ElementalProliferate_rank2 UMETA(DisplayName = "ElementalProliferate_rank2"),
	ElementalProliferate_rank3 UMETA(DisplayName = "ElementalProliferate_rank3"),
	Firewalker_rank1 UMETA(DisplayName = "Firewalker_rank1"),
	Firewalker_rank2 UMETA(DisplayName = "Firewalker_rank2"),
	Firewalker_rank3 UMETA(DisplayName = "Firewalker_rank3"),
	HotHotHot_rank1 UMETA(DisplayName = "HotHotHot_rank1"),
	HotHotHot_rank2 UMETA(DisplayName = "HotHotHot_rank2"),
	HotHotHot_rank3 UMETA(DisplayName = "HotHotHot_rank3"),
	Embers_rank1 UMETA(DisplayName = "Embers_rank1"),
	Embers_rank2 UMETA(DisplayName = "Embers_rank2"),
	Embers_rank3 UMETA(DisplayName = "Embers_rank3"),
	Pyroblast_rank1 UMETA(DisplayName = "Pyroblast_rank1"),
	Pyroblast_rank2 UMETA(DisplayName = "Pyroblast_rank2"),
	Pyroblast_rank3 UMETA(DisplayName = "Pyroblast_rank3"),
	InfernalForm_rank1 UMETA(DisplayName = "InfernalForm_rank1"),
	InfernalForm_rank2 UMETA(DisplayName = "InfernalForm_rank2"),
	InfernalForm_rank3 UMETA(DisplayName = "InfernalForm_rank3")

};

UENUM(BlueprintType)
enum class ETypeOfTalent : uint8
{
	Passive		UMETA(DisplayName = "Passive"),
	Active		UMETA(DisplayName = "Active"),
	Augment		UMETA(DisplayName = "Augment"),
	Sustained	UMETA(DisplayName = "Sustained")
};

UENUM(BlueprintType)
enum class ETypeOfTalentTree : uint8
{
	Fire		UMETA(DisplayName = "Fire"),
	Cold		UMETA(DisplayName = "Cold"),
	Acid		UMETA(DisplayName = "Acid"),
	Physical	UMETA(DisplayName = "Physical"),
	Chaos		UMETA(DisplayName = "Chaos"),
	Holy		UMETA(DisplayName = "Holy"),
};

UENUM(BlueprintType)
enum class ETypeOfCC : uint8
{
	Stun		UMETA(DisplayName = "Passive"),
	Slow		UMETA(DisplayName = "Active"),
	Freeze		UMETA(DisplayName = "Augment"),
	Knockback	UMETA(DisplayName = "Knockback")
};

UENUM(BlueprintType)
enum class ETypeOfStatusEffect : uint8
{
	Buff		UMETA(DisplayName = "Buff"),
	Debuff		UMETA(DisplayName = "Debuff"),
	Curse		UMETA(DisplayName = "Curse")
};

UENUM(BlueprintType)
enum class ETypeOfSpecialEffect : uint8
{
	//For special effects gained through talents or legendary items. stats which act in a special way
	NONE				UMETA(DisplayName = "NONE"),
	Invulnerable		UMETA(DisplayName = "Invulnerable"),
	Curse				UMETA(DisplayName = "Curse")
};


UENUM(BlueprintType)
enum class ETypeOfDOT : uint8
{
	Burning				UMETA(DisplayName = "Passive"),
	Poison				UMETA(DisplayName = "Active"),
	Bleeding			UMETA(DisplayName = "Augment")
};

UENUM(BlueprintType)
enum class ETypeOfPassive : uint8
{
	//dont remember what this was going to be used for. may be to determine if special or regular passive

	Burning				UMETA(DisplayName = "Passive"),
	Poison				UMETA(DisplayName = "Active"),
	Bleeding			UMETA(DisplayName = "Augment")
};

UENUM(BlueprintType)
enum class ETypeOfSpecialPassive : uint8
{
	//these are all the special passive effects which does more or works differently than pure stat-gain passives.
	NONE			UMETA(DisplayName = "placeholder")
};

UENUM(BlueprintType)
enum class ETypeOfEquipmentSlot : uint8
{
	NONE				UMETA(DisplayName = "No Slot"),
	Back				UMETA(DisplayName = "Back_"),
	Boots				UMETA(DisplayName = "Boots_"),
	Chest				UMETA(DisplayName = "Chest_"),
	Gloves				UMETA(DisplayName = "Gloves_"),
	Helmet				UMETA(DisplayName = "Helmet_"),
	Legs				UMETA(DisplayName = "Legs_"),
	Shoulder			UMETA(DisplayName = "Shoulder_"),
	Amulet				UMETA(DisplayName = "Amulet_"),
	Ancient_Relic		UMETA(DisplayName = "Ancient Relic_"),
	Ring_1				UMETA(DisplayName = "Ring_1"),
	Ring_2				UMETA(DisplayName = "Ring_2"),
	Flask_1				UMETA(DisplayName = "Flask_1"),
	Flask_2				UMETA(DisplayName = "Flask_2"),
	Flask_3				UMETA(DisplayName = "Flask_3"),
	Mainhand			UMETA(DisplayName = "Main-hand_"),
	Offhand				UMETA(DisplayName = "Off-hand_")
};

UENUM(BlueprintType)
enum class ETypeOfResource : uint8
{
	Health		UMETA(DisplayName = "Health"),
	Mana		UMETA(DisplayName = "Mana")
};

UENUM(BlueprintType)
enum class EStandardItemPrefix : uint8
{
	Fiery				UMETA(DisplayName = "Fiery"),
	Frigid				UMETA(DisplayName = "Frigid"),
	Reinforced			UMETA(DisplayName = "Reinforced")
};

//Helper function creates a TArray of all values of an enum that you can use to loop through
//Credit: "Da Stranger" on unreal forums - url:https://answers.unrealengine.com/questions/235545/c-static-array-of-enum.html
//usage: auto lEnums = EnumGetList<EMyEnum>(TEXT("EMyEnum"));
//for (auto i : lEnums)
template<typename T>
TArray<T> EnumGetList(const FString& enumName)
{
	TArray<T> lResult;
	UEnum* pEnum = FindObject<UEnum>(ANY_PACKAGE, *enumName, true);
	for (int i = 0; i <= pEnum->GetMaxEnumValue(); ++i)
	{
		if (pEnum->IsValidEnumValue(i))
			lResult.Add(static_cast<T>(i));
	}
	return lResult;
}