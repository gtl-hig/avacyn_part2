// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Character.h"
#include "Damageable.h"
#include "Healable.h"
#include "AvacynCharacter.generated.h"

UCLASS(Blueprintable)
class AAvacynCharacter : public ACharacter, public IDamageable, public IHealable
{
	GENERATED_BODY()

public:
	AAvacynCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }
	// Returns Statistics subobject
	FORCEINLINE class UStatistics* GetStatisticsComponent() const { return Statistics; }
	// Returns Spellcasting subobject
	FORCEINLINE class USpellCastingComponent* GetSpellCastingComponent() const { return SpellCastingComponent; }
	// Returns Inventory subobject
	FORCEINLINE class UInventory* GetInventoryComponent() const { return Inventory;  }
	// returns Equipped items subobject
	FORCEINLINE class UEquippedItems* GetEquippedItemsComponent() const { return EquippedItems; }
	// returns Equipped items subobject
	FORCEINLINE class UTalentTree* GetTalentTreeComponent() const { return TalentTree; }
	// Set Inventory component
	FORCEINLINE void SetInventoryComponent(UInventory* NewInventory) { Inventory = NewInventory; }
	
	// Controllers call this to make a character cast spells
	void CastSpell(ESpellName Spell, FVector TargetLocation);

	//ReceiveDamage interface
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interface")
	bool ReceiveDamage(FProjectileDamageData ProjectileDamageData);
	virtual bool ReceiveDamage_Implementation(FProjectileDamageData ProjectileDamageData) override;

	//ReceiveHealing interface
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interface")
	bool ReceiveHealing();
	virtual bool ReceiveHealing_Implementation() override;

	///** Returns Maximum life */
	//FORCEINLINE float GetMaximumLife() { return MaximumLife; }
	///** Returns Maximum mana */
	//FORCEINLINE float GetMaximumMana() { return MaximumMana; }
	///** Returns Current health */
	//FORCEINLINE float GetCurrentLife() { return CurrentLife; }
	///** Returns Maximum life */
	//FORCEINLINE float GetCurrentMana() { return CurrentMana; }

	UAnimationAsset* MyAnimation;

	void EnterSpellcastingState(float Time);

	void ExitSpellcastingState();

private:

	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;
	
	/** Top down camera reference for when switching to first person. need the reference in order to regain the proper camera afterwards. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* CameraReference;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

	// Statistics component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Statistics", meta = (AllowPrivateAccess = "true"))
	class UStatistics* Statistics;

	// SpellCasting component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Spells", meta = (AllowPrivateAccess = "true"))
	class USpellCastingComponent* SpellCastingComponent;

	// Inventory Component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory", meta = (AllowPrivateAccess = "true"))
	class UInventory* Inventory;

	// EquippedItem component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "EquippedItem", meta = (AllowPrivateAccess = "true"))
	class UEquippedItems* EquippedItems;

	// EquippedItem component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Talent Tree", meta = (AllowPrivateAccess = "true"))
	class UTalentTree* TalentTree;

	FTimerHandle SpellcastingState;
};

