// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "Enums.h"
#include "Structs.h"
#include "AvacynCharacter.h"
//#include "BaseEnemy.h"
#include "Statistics.generated.h"


UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class AVACYN_API UStatistics : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UStatistics();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Statistics")
	float GetStat(ETypeOfStat stat);
	
	UFUNCTION(BlueprintCallable, Category = "Statistics")
	void ModifyStat(ETypeOfStat stat, float value);

	UFUNCTION(BlueprintCallable, Category = "Statistics")
	void ReceiveDamage(FProjectileDamageData ProjectileDamageData);

	UFUNCTION(BlueprintCallable, Category = "Statistics")
	void ApplyDamage(float Damage);

	UFUNCTION(BlueprintCallable, Category = "Statistics")
	bool IsElite();

	UFUNCTION(BlueprintCallable, Category = "Statistics")
	void SetIsElite(bool bIsElite);

	UFUNCTION(BlueprintCallable, Category = "Statistics")
	uint8 GetLevel();

	UFUNCTION(BlueprintCallable, Category = "Statistics")
	uint8 GetExperience();

	UFUNCTION(BlueprintCallable, Category = "Statistics")
	float GetResource(ETypeOfResource ResourceType);

	UFUNCTION(BlueprintCallable, Category = "Statistics")
	void SpendResource(ETypeOfResource ResourceType, float AmountToSpend);

private:

	static void ReceivePhysicalDamage(ETypeOfDamage DamageType, FProjectileDamageData ProjectileDamageData, UStatistics* self);
	static void ReceiveElementalDamage(ETypeOfDamage DamageType, FProjectileDamageData ProjectileDamageData, UStatistics* self);
	static void ReceiveChaosDamage(ETypeOfDamage DamageType, FProjectileDamageData ProjectileDamageData, UStatistics* self);

	// Map containing all the stats the character gets from gear
	TMap<ETypeOfStat, float> StatMap;
	
	// character
	uint16 Level;

	// character / player spesifics
	uint32 CurrentExp;

	// character / NPC spesifics
	uint32 ExpRewardOnKill;
	
	// if dual wielding
	bool bIsDualWielding;

	// is classified as elite
	bool bIsElite;

	// Map containing pointers to different functions to handle different types of damage
	TMap<ETypeOfDamage, void (*)(ETypeOfDamage, FProjectileDamageData, UStatistics*)> HandleDamageMap;

	// Helper map to map a type of damage to the resistance for that type of damage
	TMap<ETypeOfDamage, ETypeOfStat> DamageToResistanceMap;

	// Helper map to map a level to an armor value modifier
	TMap<float, float> LevelToArmorMultiplierMap;

	// Helper map to map a type of resource to the corresponding stat
	TMap<ETypeOfResource, ETypeOfStat> ResourceToStatMap;
};
