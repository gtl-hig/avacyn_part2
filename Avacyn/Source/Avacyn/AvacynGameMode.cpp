// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "Avacyn.h"
#include "AvacynGameMode.h"
//#include "AvacynGameInstance.h"
#include "AvacynPlayerController.h"
//#include "AvacynCharacter.h"
#include "Inventory.h"
#include "TalentTree.h"
#include "EquippedItems.h"
#include "Kismet/GameplayStatics.h"
#include "Runtime/Engine/Public/TimerManager.h"

AAvacynGameMode::AAvacynGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AAvacynPlayerController::StaticClass();

	//TODO: change name of pawn class?
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	//set default skeletal mesh. will be overwritten from savegame
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> MeshContainer(TEXT("SkeletalMesh'/Game/CharacterModels/InfinityBladeCharacters/SK_CharM_Barbarous.SK_CharM_Barbarous'"));
	if (MeshContainer.Succeeded())
	{
		CharacterSkeletalMesh = MeshContainer.Object;
	}

	//variable for switching menu
	bOpenNewWindow = false;

	//which menu is currently open
	CurrentlyOpen = TArray<bool>({ false, false, false, false, false, false });
	// debug settings
	bDebugMode = false;

	InitializeItemModifiers();
	InitializeStatRanges();
}

void AAvacynGameMode::BeginPlay()
{
	//getting settings
	MyGameInstance = Cast<UAvacynGameInstance>(GetGameInstance());
	if (MyGameInstance)
	{
		bDebugMode = MyGameInstance->IsDebugMode();
	}


	if (MyGameInstance->IsNewCharacter())
	{
		//create new save game 
		UAvacynSaveGame* MySaveGame = Cast<UAvacynSaveGame>(UGameplayStatics::CreateSaveGameObject(UAvacynSaveGame::StaticClass()));
		MyPawn = Cast<AAvacynCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
		if (MySaveGame)
		{
			MySaveGame->CharacterName = MyGameInstance->GetCharacterName();
			MySaveGame->PlayerLocation = UGameplayStatics::GetPlayerPawn(GetWorld(), 0)->GetActorLocation();
			MySaveGame->CharacterSkeletalMesh = MyGameInstance->GetCharacterSkeletalMesh();
			FString CharacterFileName = MyGameInstance->GetCharacterFileName();
			int32 CharacterFileSlot = MyGameInstance->GetCharacterFileSlot();
			auto Result = UGameplayStatics::SaveGameToSlot(MySaveGame, CharacterFileName, CharacterFileSlot);
			if (MyPawn)
			{
				GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Orange, TEXT("changing mesh"));
				MyPawn->GetMesh()->SetSkeletalMesh(MySaveGame->CharacterSkeletalMesh);
			}
			if (bDebugMode)
			{
				if (Result)
				{
					GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Orange, TEXT("Successfully saved on BeginPlay"));
				}
				else
				{
					GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Orange, TEXT("Failed to save on BeginPlay"));
				}
			}
			MyGameInstance->SetCharacterFileName(CharacterFileName);
			MyGameInstance->SetCharacterFileSlot(CharacterFileSlot);
		}
		// Setting the bool to false so that when going back to main menu and you want to load an existing character, 
		// it will not create a new one.
		MyGameInstance->bIsNewCharacter = false;
		//else
		//{
		//	if (bDebugMode)
		//	{
		//		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Orange, TEXT("Failed to cast save game"));
		//	}
		//}
		////debug message
		//if (bDebugMode)
		//{
		//	auto CharacterName = MyGameInstance->GetCharacterName();
		//	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Orange, CharacterName);
		//}
	}
	else
	{
		LoadCharacter();
	}
	InitializeAutoSaveGame();
}

void AAvacynGameMode::InitializeAutoSaveGame()
{
	//Set up timer to save every 5 seconds or something. call the saveCharacter function
	GetWorldTimerManager().SetTimer(TimerHandle, this, &AAvacynGameMode::SaveCharacter, 5.0f, true);
}

void AAvacynGameMode::SaveCharacter()
{
	UAvacynSaveGame* TempSave = Cast<UAvacynSaveGame>(UGameplayStatics::CreateSaveGameObject(UAvacynSaveGame::StaticClass()));
	if (TempSave)
	{
		TempSave->CharacterName = MyGameInstance->GetCharacterName();
		//TempTestSave->CharacterSkeletalMesh = CreateDefaultSubobject<USkeletalMesh>(TEXT("SkeletalMesh'/Game/CharacterModels/InfinityBladeCharacters/SK_CharM_Barbarous.SK_CharM_Barbarous'"));
		TempSave->PlayerLocation = UGameplayStatics::GetPlayerPawn(GetWorld(), 0)->GetActorLocation();
		TempSave->SaveFileName = MyGameInstance->GetCharacterFileName();
		TempSave->SaveFileSlot = MyGameInstance->GetCharacterFileSlot();
		TempSave->CharacterSkeletalMesh = MyGameInstance->GetCharacterSkeletalMesh();
		AAvacynCharacter* MyCharacter = Cast<AAvacynCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
		if (MyCharacter)
		{
			TempSave->SaveInventoryItems.Append(MyCharacter->GetInventoryComponent()->GetWholeInventory());
			TempSave->SaveTalentInfo.Append(MyCharacter->GetTalentTreeComponent()->GetAllocatedTalents());
			TempSave->SaveEquippedItems.Append(MyCharacter->GetEquippedItemsComponent()->GetEquippedItems());
		}
		else
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Purple, TEXT("Failed to get player pawn"));
		}

		//saves the saveGame to file.
		UGameplayStatics::SaveGameToSlot(TempSave, TempSave->SaveFileName, TempSave->SaveFileSlot);
		if (bDebugMode)
		{
			//auto test = TempSave->SaveTalentInfo.Num();
			//FString test2 = "num talents allocated when saving: ";
			//if (auto tmp = TempSave->SaveTalentInfo.Find(ETypeOfTalentTree::Fire))
			//{
			//	test2.AppendInt(tmp->PointsAllocated.Num());
			//}
			//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Purple, TEXT("Num talents saved: ") + test2);
			//FString UserIndexString = TEXT("");
			//UserIndexString.AppendInt(TempSave->SaveFileSlot);
			//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Purple, TEXT("Saving to: ") + TempSave->SaveFileName + TEXT(", slot: ") + UserIndexString);
			//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Purple, TEXT("Saving Player Location: ") + UGameplayStatics::GetPlayerPawn(GetWorld(), 0)->GetActorLocation().ToCompactString());
			//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Purple, TEXT("Saving Player Name: ") + TempSave->CharacterName);
		}
	}
	else
	{
		if (bDebugMode)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Purple, TEXT("Failed to save character"));

		}
	}

	//printing the data that's being saved.
}

void AAvacynGameMode::LoadCharacter()
{

	//Load character
	UAvacynSaveGame* TempSaveGame = Cast<UAvacynSaveGame>(UGameplayStatics::CreateSaveGameObject(UAvacynSaveGame::StaticClass()));
	TempSaveGame = Cast<UAvacynSaveGame>(UGameplayStatics::LoadGameFromSlot(MyGameInstance->GetCharacterFileName(), MyGameInstance->GetCharacterFileSlot()));

	if (TempSaveGame)
	{
		if (bDebugMode)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 10005.0f, FColor::Blue, TEXT("character name: ") + TempSaveGame->CharacterName);
			//GEngine->AddOnScreenDebugMessage(-1, 10005.0f, FColor::Blue, TEXT("character Location: ") + TempSaveGame->PlayerLocation.ToCompactString());
		}

		auto World = GetWorld();
		if (World)
		{
			auto Pawn = Cast<AAvacynCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
			if (Pawn)
			{
				auto Location = TempSaveGame->GetPlayerLocation();
				Pawn->SetActorLocation(TempSaveGame->PlayerLocation);

				Pawn->GetMesh()->SetSkeletalMesh(TempSaveGame->GetCharacterSkeletalMesh());
				Pawn->GetInventoryComponent()->ApplySavedItems(TempSaveGame->SaveInventoryItems);
				Pawn->GetTalentTreeComponent()->ApplyTalentInfoFromSave(TempSaveGame->SaveTalentInfo);
				Pawn->GetEquippedItemsComponent()->EquipSavedItems(TempSaveGame->SaveEquippedItems);
			}
		}
		MyGameInstance->SetCharacterName(TempSaveGame->CharacterName);
		MyGameInstance->SetCharacterFileName(TempSaveGame->SaveFileName);
		MyGameInstance->SetCharacterFileSlot(TempSaveGame->SaveFileSlot);
		MyGameInstance->SetCharacterSkeletalMesh(TempSaveGame->CharacterSkeletalMesh);
	}
}

void AAvacynGameMode::openMenu(EMenuTypes type)
{
	WindowToOpen = type;
	bOpenNewWindow = true;
}

EMenuTypes AAvacynGameMode::GetWindowToOpen()
{
	return WindowToOpen;
}

void AAvacynGameMode::SetCurrentlyOpen(EMenuTypes type, bool IsOpen)
{
	switch (type)
	{
	case EMenuTypes::MT_TalentTree:
	{
		CurrentlyOpen[0] = IsOpen;
		break;
	}
	case EMenuTypes::MT_SkillWindow:
	{
		CurrentlyOpen[1] = IsOpen;
		break;
	}
	case EMenuTypes::MT_Inventory:
	{
		CurrentlyOpen[2] = IsOpen;
		break;
	}
	case EMenuTypes::MT_EquipmentAndStats:
	{
		CurrentlyOpen[3] = IsOpen;
		break;
	}
	case EMenuTypes::MT_QuestLog:
	{
		CurrentlyOpen[4] = IsOpen;
		break;
	}
	case EMenuTypes::MT_DevelopmentTool:
	{
		CurrentlyOpen[5] = IsOpen;
		break;
	}
	default:
	{
		GEngine->AddOnScreenDebugMessage(-1, 10005.0f, FColor::Blue, TEXT("THIS SHOULD NOT PRINT: AavacynGameMode: SetCurrentlyOpen()"));
		break;
	}
	}
}

bool AAvacynGameMode::IsCurrentlyOpen(EMenuTypes type)
{

	switch (type)
	{
	case EMenuTypes::MT_TalentTree:
	{
		return CurrentlyOpen[0];
		break;
	}
	case EMenuTypes::MT_SkillWindow:
	{
		return CurrentlyOpen[1];
		break;
	}

	case EMenuTypes::MT_Inventory:
	{
		return CurrentlyOpen[2];
		break;
	}
	case EMenuTypes::MT_EquipmentAndStats:
	{
		return CurrentlyOpen[3];
		break;
	}
	case EMenuTypes::MT_QuestLog:
	{
		return CurrentlyOpen[4];
		break;
	}
	case EMenuTypes::MT_DevelopmentTool:
	{
		return CurrentlyOpen[5];
		break;
	}
	default:
	{
		GEngine->AddOnScreenDebugMessage(-1, 10005.0f, FColor::Blue, TEXT("THIS SHOULD NOT PRINT: AavacynGameMode: IsCurrentlyOpen()"));
		return false;
		break;
	}
	}

	return false;
}

FItem AAvacynGameMode::GenerateItem(ETypeOfRarity ItemRarity, int ItemLevel)
{
	FItem NewItem;
	NewItem.GUID = FGuid::NewGuid();

	auto AllItemTypes = EnumGetList<ETypeOfItem>(TEXT("ETypeOfItem"));
	int32 RandomItemType;
	do
	{
		RandomItemType = FMath::RandRange(0, AllItemTypes.Num() - 2); // .Num() returns number of enums + 1
	} while (AllItemTypes[RandomItemType] == ETypeOfItem::Consumable || AllItemTypes[RandomItemType] == ETypeOfItem::Currency);

	NewItem.Type = AllItemTypes[RandomItemType];

	NewItem.Rarity = ItemRarity;

	NewItem.LevelRequirement = ItemLevel;

	//Placeholders - start

	NewItem.Name = "NAME_PLACEHOLDER";

	NewItem.FlavorText = "";

	NewItem.ImageRefrence = "Texture2D'/Game/Images/PlaceholderMaterial.PlaceholderMaterial'";

	NewItem.MeshRefrence = "SkeletalMesh'/Game/Assets/Infinity_Blade_Weapons/SK_Blade_Sucker.SK_Blade_Sucker'";

	//Placeholders - end

	TArray<ETypeOfStat> AvailableStats;

	auto AllStandardItemPrefixes = EnumGetList<EStandardItemPrefix>(TEXT("EStandardItemPrefix"));


	int32 RandomItemPrefix = FMath::RandRange(0, AllStandardItemPrefixes.Num() - 2);
	FItemModifier Prefix = StandardItemPrefixes.FindRef(AllStandardItemPrefixes[RandomItemPrefix]);

	NewItem.Name = Prefix.NameModifier + " " + NewItem.Name;

	AvailableStats.Append(Prefix.AvailableStats);

	// Start choosing stats
	int32 RandomStat1 = FMath::RandRange(0, AvailableStats.Num() - 2);
	NewItem.StatsOnItem.Add(AvailableStats[RandomStat1], FMath::RandRange(MinStatRange.FindRef(AvailableStats[RandomStat1])[0], MinStatRange.FindRef(AvailableStats[RandomStat1])[1]));
	AvailableStats.RemoveAt(RandomStat1);

	int32 RandomStat2 = FMath::RandRange(0, AvailableStats.Num() - 2);
	NewItem.StatsOnItem.Add(AvailableStats[RandomStat2], FMath::RandRange(MinStatRange.FindRef(AvailableStats[RandomStat2])[0], MinStatRange.FindRef(AvailableStats[RandomStat2])[1]));
	AvailableStats.RemoveAt(RandomStat2);

	return NewItem;
}

void AAvacynGameMode::InitializeItemModifiers()
{
	FItemModifier Fiery;
	Fiery.NameModifier = "Fiery";
	Fiery.AvailableStats.Add(ETypeOfStat::UTIL_MovementSpeed);
	Fiery.AvailableStats.Add(ETypeOfStat::OFF_ProjectileSpeed);
	Fiery.AvailableStats.Add(ETypeOfStat::OFF_ChanceToIgniteOnHit);
	Fiery.AvailableStats.Add(ETypeOfStat::DI_FireDamage);
	Fiery.AvailableStats.Add(ETypeOfStat::DP_DamagePenetrationFire);
	StandardItemPrefixes.Add(EStandardItemPrefix::Fiery, Fiery);

	FItemModifier Frigid;
	Frigid.NameModifier = "Frigid";
	Frigid.AvailableStats.Add(ETypeOfStat::RES_MaxMana);
	Frigid.AvailableStats.Add(ETypeOfStat::RES_ManaPerSecond);
	Frigid.AvailableStats.Add(ETypeOfStat::OFFM_SpellCritChance);
	Frigid.AvailableStats.Add(ETypeOfStat::OFFM_SpellCritDamage);
	Frigid.AvailableStats.Add(ETypeOfStat::DI_FrostDamage);
	StandardItemPrefixes.Add(EStandardItemPrefix::Frigid, Frigid);

	FItemModifier Reinforced;
	Reinforced.NameModifier = "Reinforced";
	Reinforced.AvailableStats.Add(ETypeOfStat::DEF_Armor);
	Reinforced.AvailableStats.Add(ETypeOfStat::DEF_DodgeChance);
	Reinforced.AvailableStats.Add(ETypeOfStat::DR_DamageResistancePhysical);
	Reinforced.AvailableStats.Add(ETypeOfStat::DR_DamageResistanceFire);
	Reinforced.AvailableStats.Add(ETypeOfStat::DR_DamageResistanceFrost);
	Reinforced.AvailableStats.Add(ETypeOfStat::DR_DamageReduction);
	StandardItemPrefixes.Add(EStandardItemPrefix::Reinforced, Reinforced);
}

void AAvacynGameMode::InitializeStatRanges()
{
	TArray<float> NewMinStatRange;
	NewMinStatRange.Reserve(2);
	NewMinStatRange.Add(1.0f);
	NewMinStatRange.Add(1.0f);

	NewMinStatRange[0] = 0.05f;
	NewMinStatRange[1] = 0.1f;
	MinStatRange.Add(ETypeOfStat::UTIL_MovementSpeed, NewMinStatRange);

	NewMinStatRange[0] = 0.05f;
	NewMinStatRange[1] = 0.1f;
	MinStatRange.Add(ETypeOfStat::OFF_ProjectileSpeed, NewMinStatRange);

	NewMinStatRange[0] = 0.02f;
	NewMinStatRange[1] = 0.08f;
	MinStatRange.Add(ETypeOfStat::OFF_ChanceToIgniteOnHit, NewMinStatRange);

	NewMinStatRange[0] = 0.05f;
	NewMinStatRange[1] = 0.2f;
	MinStatRange.Add(ETypeOfStat::DI_FireDamage, NewMinStatRange);

	NewMinStatRange[0] = 0.02f;
	NewMinStatRange[1] = 0.07f;
	MinStatRange.Add(ETypeOfStat::DP_DamagePenetrationFire, NewMinStatRange);

	NewMinStatRange[0] = 100.0f;
	NewMinStatRange[1] = 400.0f;
	MinStatRange.Add(ETypeOfStat::RES_MaxMana, NewMinStatRange);

	NewMinStatRange[0] = 5.0f;
	NewMinStatRange[1] = 20.0f;
	MinStatRange.Add(ETypeOfStat::RES_ManaPerSecond, NewMinStatRange);

	NewMinStatRange[0] = 0.03f;
	NewMinStatRange[1] = 0.08f;
	MinStatRange.Add(ETypeOfStat::OFFM_SpellCritChance, NewMinStatRange);

	NewMinStatRange[0] = 0.1f;
	NewMinStatRange[1] = 0.5f;
	MinStatRange.Add(ETypeOfStat::OFFM_SpellCritDamage, NewMinStatRange);

	NewMinStatRange[0] = 0.05f;
	NewMinStatRange[1] = 0.2f;
	MinStatRange.Add(ETypeOfStat::DI_FrostDamage, NewMinStatRange);

	NewMinStatRange[0] = 10.0f;
	NewMinStatRange[1] = 40.0f;
	MinStatRange.Add(ETypeOfStat::DEF_Armor, NewMinStatRange);

	NewMinStatRange[0] = 0.02f;
	NewMinStatRange[1] = 0.08f;
	MinStatRange.Add(ETypeOfStat::DEF_DodgeChance, NewMinStatRange);

	NewMinStatRange[0] = 0.02f;
	NewMinStatRange[1] = 0.08f;
	MinStatRange.Add(ETypeOfStat::DR_DamageResistancePhysical, NewMinStatRange);

	NewMinStatRange[0] = 0.02f;
	NewMinStatRange[1] = 0.08f;
	MinStatRange.Add(ETypeOfStat::DR_DamageResistanceFire, NewMinStatRange);

	NewMinStatRange[0] = 0.02f;
	NewMinStatRange[1] = 0.08f;
	MinStatRange.Add(ETypeOfStat::DR_DamageResistanceFrost, NewMinStatRange);

	NewMinStatRange[0] = 0.02f;
	NewMinStatRange[1] = 0.08f;
	MinStatRange.Add(ETypeOfStat::DR_DamageReduction, NewMinStatRange);
}