// Fill out your copyright notice in the Description page of Project Settings.

#include "Avacyn.h"
#include "AvacynGameInstance.h"
#include "SpellDetails.h"
#include "Fireball.h"
#include "Flamestrike.h"
#include "AvacynCharacter.h"
#include "BaseEnemy.h"
#include "SpellCastingComponent.h"


// Sets default values for this component's properties
USpellCastingComponent::USpellCastingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// Construct the DamageToResistanceMap
	{
		DamageToPenetrationMap.Add(ETypeOfDamage::Physical, ETypeOfStat::DP_DamagePenetrationPhysical);
		DamageToPenetrationMap.Add(ETypeOfDamage::Fire, ETypeOfStat::DP_DamagePenetrationFire);
		DamageToPenetrationMap.Add(ETypeOfDamage::Frost, ETypeOfStat::DP_DamagePenetrationFrost);
		DamageToPenetrationMap.Add(ETypeOfDamage::Lightning, ETypeOfStat::DP_DamagePenetrationLightning);
		DamageToPenetrationMap.Add(ETypeOfDamage::Holy, ETypeOfStat::DP_DamagePenetrationHoly);
		DamageToPenetrationMap.Add(ETypeOfDamage::Shadow, ETypeOfStat::DP_DamagePenetrationShadow);
		DamageToPenetrationMap.Add(ETypeOfDamage::Nature, ETypeOfStat::DP_DamagePenetrationNature);
		DamageToPenetrationMap.Add(ETypeOfDamage::Poison, ETypeOfStat::DP_DamagePenetrationPoison);
		DamageToPenetrationMap.Add(ETypeOfDamage::Acid, ETypeOfStat::DP_DamagePenetrationAcid);
	}

	// Initialize all spells as unavailable
	auto AllSpellNameEnums = EnumGetList<ESpellName>(TEXT("ESpellName"));
	for (auto i : AllSpellNameEnums)
	{
		HasAccessToSpell.Add(i, false);
	}
}


// Called when the game starts
void USpellCastingComponent::BeginPlay()
{
	Super::BeginPlay();
	SetSpellAccess(ESpellName::Fireball_rank1, true);
	SetSpellAccess(ESpellName::Fireball_rank2, true);
	SetSpellAccess(ESpellName::Fireball_rank3, true);
	SetSpellAccess(ESpellName::Flamestrike_rank1, true);
	SetSpellAccess(ESpellName::Flamestrike_rank2, true);
	SetSpellAccess(ESpellName::Flamestrike_rank3, true);
}


// Called every frame
void USpellCastingComponent::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	if (TickType == ELevelTick::LEVELTICK_All)
	{
		TickSpellCooldowns(DeltaTime);
	}
}

bool USpellCastingComponent::CastSpell(ESpellName Spell, FVector TargetLocation)
{
	AActor* MyPawn = GetOwner();
	if (MyPawn)
	{
		if (!HasAccessToSpell.FindRef(Spell))
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, TEXT("Does not have access to spell!"));
			return false;
		}

		if (RemainingSpellCooldown.FindRef(Spell) > 0.0f)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, TEXT("Spell is on cooldown!"));
			return false;
		}

		AAvacynCharacter* PlayerPawn = Cast<AAvacynCharacter>(MyPawn);
		ABaseEnemy* EnemyPawn = Cast<ABaseEnemy>(MyPawn);

		if (PlayerPawn)
		{
			if (!UseResource(PlayerPawn, Spell))
			{
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, TEXT("Does not have enough resource!"));
				return false;
			}
		}
		else if (EnemyPawn)
		{
			if (!UseResource(EnemyPawn, Spell))
			{
				return false;
			}
		}

		TMap<ESpellName, FSpellInfo> SpellDetails = Cast<UAvacynGameInstance>(MyPawn->GetGameInstance())->GetSpellDetailsComponent()->GetSpellInfo();

		// Tell character to use correct spellcasting animation and make it unable to move if spell requires this

		if (SpellDetails.FindRef(Spell).CastTime == 0.0f)
		{
			FinishCastingSpell(Spell, TargetLocation);
		}
		else
		{
			if (PlayerPawn)
			{
				PlayerPawn->EnterSpellcastingState(SpellDetails.FindRef(Spell).CastTime);
			}
			FTimerDelegate TimerDelegate = FTimerDelegate::CreateUObject(this, &USpellCastingComponent::FinishCastingSpell, Spell, TargetLocation);
			GetWorld()->GetTimerManager().SetTimer(CurrentSpellcast, TimerDelegate, SpellDetails.FindRef(Spell).CastTime, false);
		}
		

		return true;
	}

	return false;
}

void USpellCastingComponent::FinishCastingSpell(ESpellName Spell, FVector TargetLocation)
{
	AActor* MyPawn = GetOwner();
	if (MyPawn)
	{
		AAvacynCharacter* PlayerPawn = Cast<AAvacynCharacter>(MyPawn);
		ABaseEnemy* EnemyPawn = Cast<ABaseEnemy>(MyPawn);

		// find actor location, target location and determin rotation for the source actor towards the target
		FVector PawnLocation = MyPawn->GetActorLocation();
		TargetLocation.Z += (PawnLocation.Z - TargetLocation.Z);
		FVector Rotation(TargetLocation - PawnLocation);
		Rotation.Normalize();

		TMap<ESpellName, FSpellInfo> SpellDetails = Cast<UAvacynGameInstance>(MyPawn->GetGameInstance())->GetSpellDetailsComponent()->GetSpellInfo();

		FActorSpawnParameters SpawnInfo;
		SpawnInfo.Instigator = Cast<APawn>(MyPawn);

		FProjectileBehaviorData NewProjectileBehaviorData;
		NewProjectileBehaviorData.MaxTravelDistance = SpellDetails.FindRef(Spell).Range;
		NewProjectileBehaviorData.ProjectileSpeed = SpellDetails.FindRef(Spell).ProjectileSpeed;
		NewProjectileBehaviorData.AoESize = SpellDetails.FindRef(Spell).AoESize;

		FProjectileDamageData NewProjectileDamageData;
		NewProjectileDamageData.Instigator = MyPawn;

		SpellDetails.FindRef(Spell).CastTime;

		float Damage = 0.0f;
		float AoEModifier = 1.0f;
		if (PlayerPawn)
		{
			CalculateDamageData(PlayerPawn, NewProjectileDamageData, Damage, Spell);
			AoEModifier = PlayerPawn->GetStatisticsComponent()->GetStat(ETypeOfStat::UTIL_AoeSize);
			NewProjectileBehaviorData.AoESize = SpellDetails.FindRef(Spell).AoESize * AoEModifier;
		}
		else if (EnemyPawn)
		{
			CalculateDamageData(EnemyPawn, NewProjectileDamageData, Damage, Spell);
			AoEModifier = EnemyPawn->GetStatisticsComponent()->GetStat(ETypeOfStat::UTIL_AoeSize);
			NewProjectileBehaviorData.AoESize = SpellDetails.FindRef(Spell).AoESize * AoEModifier;
		}

		if (SpellDetails.FindRef(Spell).ClassToSpawn == "FIREBALL")
		{
			NewProjectileDamageData.Damage.Add(ETypeOfDamage::Fire, Damage);

			FTransform ProjectileTransform = FTransform(Rotation.Rotation(), PawnLocation, FVector(1.0f, 1.0f, 1.0f));

			auto Projectile = MyPawn->GetWorld()->SpawnActorDeferred<AFireball>(AFireball::StaticClass(), ProjectileTransform);
			Projectile->SetProjectileData(NewProjectileBehaviorData, NewProjectileDamageData);
			Projectile->Initialize();
			Projectile->FinishSpawning(ProjectileTransform);
		}
		else if (SpellDetails.FindRef(Spell).ClassToSpawn == "FLAMESTRIKE")
		{
			NewProjectileDamageData.Damage.Add(ETypeOfDamage::Fire, Damage);

			FTransform ProjectileTransform = FTransform(Rotation.Rotation(), TargetLocation, FVector(1.0f, 1.0f, 1.0f));

			auto Projectile = MyPawn->GetWorld()->SpawnActorDeferred<AFlamestrike>(AFlamestrike::StaticClass(), ProjectileTransform);
			Projectile->SetProjectileData(NewProjectileBehaviorData, NewProjectileDamageData);
			Projectile->Initialize();
			Projectile->FinishSpawning(ProjectileTransform);
		}	
		else
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("ERROR: SpellCastingComponent tried to cast spell not in its switch statement!"));
		}

		if (PlayerPawn)
		{
			PutSpellOnCooldown(PlayerPawn, Spell);
		}
		else if (EnemyPawn)
		{
			PutSpellOnCooldown(EnemyPawn, Spell);
		}
	}

	// Tell character it finished casting, and allow it to move normally again
}

template<typename T>
void USpellCastingComponent::CalculateDamageData(T Character, FProjectileDamageData &NewProjectileDamageData, float &Damage, ESpellName Spell)
{
	auto AllTypesOfDamageEnums = EnumGetList<ETypeOfDamage>(TEXT("ETypeOfDamage"));

	NewProjectileDamageData.InstigatorLevel = Character->GetStatisticsComponent()->GetLevel();
	NewProjectileDamageData.EliteDamageModifier = Character->GetStatisticsComponent()->GetStat(ETypeOfStat::OFF_EliteDamage);
	for (auto i : AllTypesOfDamageEnums)
	{
		if (i != ETypeOfDamage::Bleed && i != ETypeOfDamage::Chaos)
		{
			NewProjectileDamageData.DamagePenetrationPercentage.Add(i, Character->GetStatisticsComponent()->GetStat(DamageToPenetrationMap.FindRef(i)));
		}
	}

	TMap<ESpellName, FSpellInfo> SpellDetails = Cast<UAvacynGameInstance>(Character->GetGameInstance())->GetSpellDetailsComponent()->GetSpellInfo();

	float MinDamage =
		SpellDetails.FindRef(Spell).BaseDamageMin *
		Character->GetStatisticsComponent()->GetStat(ETypeOfStat::OFFM_BaseMagicPower) *
		Character->GetStatisticsComponent()->GetStat(ETypeOfStat::OFFM_MagicPower);

	float MaxDamage =
		SpellDetails.FindRef(Spell).BaseDamageMax *
		Character->GetStatisticsComponent()->GetStat(ETypeOfStat::OFFM_BaseMagicPower) *
		Character->GetStatisticsComponent()->GetStat(ETypeOfStat::OFFM_MagicPower);

	Damage = FMath::FRandRange(MinDamage, MaxDamage);

	if (FMath::FRandRange(0.0f, 1.0f) > 1.0f - Character->GetStatisticsComponent()->GetStat(ETypeOfStat::OFFM_SpellCritChance))
	{
		Damage *= Character->GetStatisticsComponent()->GetStat(ETypeOfStat::OFFM_SpellCritDamage);
	}
}

void USpellCastingComponent::TickSpellCooldowns(float DeltaTime)
{
	for (auto i : RemainingSpellCooldown)
	{
		if (i.Value != 0.0f)
		{
			i.Value -= DeltaTime;

			if (i.Value < 0.0f)
			{
				i.Value = 0.0f;
			}

			RemainingSpellCooldown.Add(i.Key, i.Value);
		}
	}
}

void USpellCastingComponent::SetSpellCooldown(ESpellName Spell, float Duration)
{
	RemainingSpellCooldown.Add(Spell, Duration);
}

void USpellCastingComponent::SetSpellAccess(ESpellName Spell, bool bNewHasAccessToSpell)
{
	HasAccessToSpell.Add(Spell, bNewHasAccessToSpell);
	if (bNewHasAccessToSpell)
	{
		if (RemainingSpellCooldown.Find(Spell) == nullptr)
		{
			SetSpellCooldown(Spell, 0.0f);
		}
	}
}

template<typename T>
bool USpellCastingComponent::UseResource(T Character, ESpellName Spell)
{
	TMap<ESpellName, FSpellInfo> SpellDetails = Cast<UAvacynGameInstance>(Character->GetGameInstance())->GetSpellDetailsComponent()->GetSpellInfo();

	float CurrentAmountOfResource = Character->GetStatisticsComponent()->GetResource(SpellDetails.FindRef(Spell).ResourceType);

	if (CurrentAmountOfResource > SpellDetails.FindRef(Spell).ResourceCost)
	{
		Character->GetStatisticsComponent()->SpendResource(SpellDetails.FindRef(Spell).ResourceType, SpellDetails.FindRef(Spell).ResourceCost);
		return true;
	}

	return false;
}

template<typename T>
void USpellCastingComponent::PutSpellOnCooldown(T Character, ESpellName Spell)
{
	TMap<ESpellName, FSpellInfo> SpellDetails = Cast<UAvacynGameInstance>(Character->GetGameInstance())->GetSpellDetailsComponent()->GetSpellInfo();

	RemainingSpellCooldown.Add(Spell, SpellDetails.FindRef(Spell).Cooldown * (1 - Character->GetStatisticsComponent()->GetStat(ETypeOfStat::UTIL_CooldownReduction)));
}

void USpellCastingComponent::StopCastingSpell()
{
	FTimerDelegate TimerDelegate = FTimerDelegate::CreateUObject(this, &USpellCastingComponent::FinishCastingSpell, ESpellName::Fireball_rank1, FVector(0, 0, 0));
	GetWorld()->GetTimerManager().SetTimer(CurrentSpellcast, TimerDelegate, 0.0f, false);
}