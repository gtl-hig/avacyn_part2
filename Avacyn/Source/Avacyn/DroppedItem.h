// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Structs.h"
#include "GameFramework/Actor.h"
#include "DroppedItem.generated.h"

UCLASS()
class AVACYN_API ADroppedItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADroppedItem();
	void InitializeDroppedItemData(FItem DroppedItemData, FVector PositionToSpawn);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(Category = DroppedItem, VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	USkeletalMeshComponent* MySkeletalMesh;

	//UPROPERTY(Category = DroppedItem, VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	//USphereComponent* SphereComponent;
	
	UPROPERTY(Category = DroppedItem, VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	UBoxComponent* BoxComponent;
	FItem ItemData;
	
	UFUNCTION(BlueprintCallable, meta = (AllowPrivateAccess = "true"))
	FItem GetItemData();

	UFUNCTION(BlueprintCallable, meta = (AllowPrivateAccess = "true"))
	void SetItemData(FItem DataToSet);
};
