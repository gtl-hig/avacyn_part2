// Fill out your copyright notice in the Description page of Project Settings.

#include "Avacyn.h"
#include "DroppedItem.h"


// Sets default values
ADroppedItem::ADroppedItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	//SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("RootComponent"));
	//RootComponent = SphereComponent;
	//SphereComponent->InitSphereRadius(40.0f);
	//SphereComponent->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
	//SphereComponent->BodyInstance.SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	//SphereComponent->SetSimulatePhysics(true);
	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("RootComponent"));
	RootComponent = BoxComponent;
	BoxComponent->InitBoxExtent(FVector(5.0f, 25.0f, 80.0f));
	BoxComponent->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
	BoxComponent->BodyInstance.SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	BoxComponent->SetSimulatePhysics(true);
	MySkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Dropped Item Skeletal Mesh Component"));
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> MeshContainer(TEXT("SkeletalMesh'/Game/Assets/Infinity_Blade_Weapons/SK_Blade_Sucker.SK_Blade_Sucker'"));
	if (MeshContainer.Succeeded())
	{
		MySkeletalMesh->SetSkeletalMesh(MeshContainer.Object);
		MySkeletalMesh->SetupAttachment(RootComponent);
		static ConstructorHelpers::FObjectFinder<UMaterial>		Material(TEXT("Material'/Game/Images/PlaceholderMaterial_Mat.PlaceholderMaterial_Mat'"));
		if (Material.Succeeded())
		{
			MySkeletalMesh->SetMaterial(0, Material.Object);
		}
	}
}

void ADroppedItem::InitializeDroppedItemData(FItem DroppedItemData, FVector PositionToSpawn)
{
	this->ItemData = DroppedItemData;
	SetActorLocation(PositionToSpawn);
}

// Called when the game starts or when spawned
void ADroppedItem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADroppedItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//GEngine->AddOnScreenDebugMessage(-1, 0.2f, FColor::Red, TEXT("Dropped Item tick::item name: ") + ItemData.Name);

}

FItem ADroppedItem::GetItemData()
{
	return ItemData;
}

void ADroppedItem::SetItemData(FItem DataToSet)
{
	ItemData = DataToSet;
}