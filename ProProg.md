# ProProg #

## Students: ##

- Kristian Gregersen
- Sebastian Kolbu

## Other repos: ##
[Avacyn](https://bitbucket.org/gtl-hig/avacyn) - Our original repo. We switched away from this during the project because it got too big after we uploaded a bunch of media files (textures/meshes etc.)

## Group Discussion ##

#### Strengths and weaknesses of languages you used in your project ####
##### Blueprints #####
Is a visual scripting language created by the epic team for the Unreal Engine. It is designed with simplicity in mind, making it as accessible as possible for non-programmers.
 
###### Strength: ######
- Very fast to write simple functionality.
- Easy to use even for non-programmers.
- Live debugging of the script makes it easy to find flaws.


###### Weakness: ######
- Not all functionality found in the base language is present in this scripting language, and finding workarounds for this can be both time consuming and un-optimal.
- Hard to optimize.
- Blueprints can easily become very messy and complex, making it hard to see what’s actually going on in the code.
- Communicating with C++ code is easy, but the other way around is slightly more difficult.
 
##### C++ #####
###### Strength: ######
- Well suited for game development.
- Allows low level control for optimizations.
- Very good runtime performance.
- You can do imperative, functional, declarative and OO style programming.
- C++ is broadly used, and will most likely stick around for a long time.
 
###### Weakness: ######
- Not very safe, requires failsafe checks in order to avoid reading part of memory that is out of bounds.
- By default there is no built-in memory management, requiring developers to use external libraries or re-invent the wheel.

#### How you controlled process and communication systems during development ####

We did not have sprint meetings, instead we assigned tasks and issues as they were created. We felt this was a better way to spend our time as we were only two working on the game, and had a continuous communication informing the other person what was being worked on. 
 
For communication we mainly used Discord. We had several voice channels set up, everyone had their own one, and we had a lobby channel. The personal once were used during working hours so we could all be available to each other for questions, and the lobby channel was where we met up for our daily ‘coffee’ meetings in the morning, had our meetings, or if where we hung out when we simply wanted to work together on something.
We also had different text channels on our discord server. One for general communication and question asking, a few with different useful information, like links, git commands and function calls we had created that others might want to use.

#### Use of version control systems, ticket tracking, branching, version control ####

We used Git as our version control system. The repository is stored on an online repository provider, [bitbucket](https://bitbucket.org/). We were to commit as often as possible, describing what we were working on with smart commits, linking the commits to tasks in JIRA. Once the code we developed was working, we would push the code to the repository. 
Issue tracking:
JIRA was used for tracking issues, tasks, and bugs. This was so that we could manage these tickets with the sprints we were running. We also used discord as a pseudo issue tracker for issues which were simple and would take 30 seconds to fix, not worthy of its own issue on JIRA.
Branching:
A new branch was created for each separate game system. The name of the branches would describe which system is being worked on, such as Spells, GUI-Inventory, etc. One branch belonged to a single developer, however the developer is not a part of the name as we were only two people and knew which branch belonged to who.

#### Link to programming style guide including how to comment code. ####

As we were working with the Unreal Engine, we decided to use [their coding standard](https://docs.unrealengine.com/latest/INT/Programming/Development/CodingStandard/).

#### Use of libraries and integration of libraries ####

For this project we only used libraries created by the epic team for the Unreal Engine, these were all already available in the project when we created it. As we were developing a game using the Unreal Engine, it felt correct to make use of the special classes and variables created by the epic team.

#### Professionalism in your approach to software development ####

We used the Scrum development methodology for our project. We had meetings at the start of our two week sprints and determined what we would work on for the following sprint. We also had daily ‘coffee’ meetings in the morning, where we would talk and discuss what we were to do on that day.

#### Use of code review ####

Each Friday we would have a code review where all group members would show the code they had written that week to the rest of the group, and we would look for things like; Coding standard not being followed, potential flaws or better ways to write the code. This would help maintain a level of consistency in our work, and better share understanding of the code and learn from each other.
However, as the project went on we felt we were progressively more pressed for time. We started prioritizing coding over code review, as we had so much we needed to implement.

## Individual Discussion ##

### Kristian Gregersen ###

#### A link to, and discussion of, code you consider good. ####

For the most part I consider the code in the [SpellCastingComponent.cpp](Avacyn/Source/Avacyn/SpellCastingComponent.cpp) / [SpellCasting Component.h](Avacyn/Source/Avacyn/SpellCastingComponent.h) to be pretty well written. I would possibly look into templating the code a bit more, to reduce the amount of checks and mostly similar code I have written for doing the same thing to either the player character or the enemy performing the action, though I have already done this for the majority of the function to cast spells. I would also look into commenting the code a bit more, as it currently isn’t that well commented.

#### A link to, and discussion of, code you consider bad. ####

I consider the current implementation of the AI to be pretty bad. Especially so up until a recent fix. I didn’t fully understand everything about how exactly the behavior tree algorithm worked, which lead to a lot of confusion and bad implementation. Most notably I had used a selector node where I should have been using a sequencer node, and ended up having to return the opposite of what I should have to achieve the correct behavior. There is also some hard coded variables (the DesiredRangeToTarget) which should be a variable in the blackboard. In fairness, the AI is by no means done, and this would be fixed in future updates, after the part of actually making the AI select a spell to use were implemented.

[BTTask_MoveToPlayer.cpp](Avacyn/Source/Avacyn/BTTask_MoveToPlayer.cpp) - a task node for the behavior tree that shows the mistakes mentioned above.

These flaws can also be view in the [Behavior Tree](Avacyn/Content/Enemies/EnemyBT.uasset) if you pull versions of the project from April or earlier.
 
Overall I feel the commenting could have been a bit better. I for the most part commented functions and variables in the .h files, to explain what they do/are there for. But explaining complicated things I do in code could be useful for other people reading through my code.

#### A personal reflection about professionalism in programming ####

During this project I focused a lot on making sure the systems I implemented would be scalable, flexible, and robust. In the end I do feel like I have accomplished this, but there are still a lot of things I need to work on. I feel like I need more descriptive commit messages in some cases, commit more often, and also remember to use smart commits to link my changes to issues/tasks. More consistently remember to comment important sections of my code. Also, I spent a lot of time trying to think of how to best implement systems, I think this is good, but I should get better at writing down/drawing this, both for speeding up the process on my end, and to let others know how I want it to work.

### Sebastian Kolbu ###

#### A link to, and discussion of, code you consider good. ####

Equipped items part of the GUI, consisting of

- BP_EquipmentAndStats
- BP_EquipmentSlot
- [EquippedItems.cpp](Avacyn/Source/Avacyn/EquippedItems.cpp)
- [Inventory.cpp](Avacyn/Source/Avacyn/Inventory.cpp)

There are some bad aspects in these blueprints and cpp files, however I still consider the implementation of how to handle equipping and unequipping inventory items to be a good solution. How to handle data is important, and the implementation I did makes it so that the blueprint does not alter any information, it merely displays it. Whenever an action requires the information to change, such as equipping a new item, the equipmentAndStats blueprint sends an equip request to equippedItems component of the character, letting it know that the player wants to equip an item, passing along the which slot and the Guid of the item it wants to equip. This way, the blueprint stores as little information as possible of the data itself.

#### A link to, and discussion of, code you consider bad. ####

The code I consider bad can be found in the blueprints BP_MainMenu and BP_CharacterButton.

With little knowledge of how to work with blueprints properly, I did some implementations which are not very optimal. 

- For one, the entire main menu is located within one single blueprint file. This makes it very hard to continue work on the GUI as elements are overlapping and hard to modify due to the incredibly long hierarchy of content. 
- The functions are not utilizing the Sequence node. This results in the code stretching horizontally and is not very easy to read. 
- Commenting is non-existent, which is also bad as some complex functionality is performed in the blueprint- like getting all saved characters and creating a list of them to display. 
 
Possible improvements that can be done:

- divide the different menus into separate blueprint files. I would create a widget blueprint which takes care of the styling of the menu. This blueprint would be used to create all the different menus programmatically. For functionality that is unique to the different sub-menus, such as game settings, I would create a separate blueprint for each of these. This way it is simple to modify and adjust.
- the functionality for switching visibility of the different menus for navigating through the menu could be improved from making everything else invisible, and the desired menu visible, to everything being invisible by default, and toggling the desired menu. This would also be done by having a single function which takes an enum as input, the enum represents which menu to show. This would remove the duplication of code that is present in the blueprint.
- Change the list of mesh (in character creation) to be a single dropdown menu or create a separate blueprint which holds information about a single mesh and when clicked, changes the mesh of the model displayed. This way I could programmatically create these widgets depending on how many meshes we have available, instead of hard coding them into the menu. It would also be easier to style them as I would no longer have to change each button separately. 

#### A personal reflection about professionalism in programming ####

I don’t consider myself a true professional programmer. The reason for this is because I feel I do so many mistakes and forget to do important things, such as commenting and maintaining coding convention. However, throughout this project I feel have improved greatly. I always tried to use descriptive names for variables and functions, making it easy to see what a function or variable is used for at a glance. 

One thing I could do better would be to plan the systems I’m about to implement better. Too often I just get to work, and halfway through I realize that I need to make changes in order for the system to work. This results in me having to do more work by refactoring. This was often due to the implementation not being scalable enough or just did things wrong. The planning I did was done using my physical notebook, writing down which functionality I wanted to implement in the system, and often the way I wanted the system to look.
 
My commit messages are not always as descriptive as they could be. I don’t commit often enough, so the commit messages becomes a giant list of things done, or I don’t list all the things that were done. Early on in the project I used smart commit messages, linking the commits to tasks in the issue tracker. However, as we went on and felt we pressed on time, I did less smart commits as the new tasks were not created in the issue tracker. This is something I need to work more on.